# Protocol Pioneer Chapter 5

## Introduction

This lab was setting up a strategy to get the best success rate for sending and receiving transmission between routers when considering interference.

## Collaboration

I collaborated with Alex Traynor and Soleil Xie for this lab.

## Process

I followed the instructions on the [course website](https://ic322.com/#/assignments/week13/protocol-pioneer/) for this lab.

## Questions
**1. Include your client and server strategy code.**
- Both the client and server used the same strategy
```
import json
import math

def create_message(src, dest, msg_type, payload):
    """If `payload` is a string, it will come back out as a string. If it's a
    Dict, it will come back out as a dict."""
    
    m = {
        "Source": src,
        "Destination": dest,
        "Type": msg_type,
        "Payload": payload
    }

    return json.dumps(m)

def parse_message(m):
    parsed = json.loads(m)
    return {
        "Source": parsed["Source"],
        "Destination": parsed["Destination"],
        "Type": parsed["Type"],
        "Payload": parsed["Payload"]
    }

def compress_mes(m):
    msg = parse_message(m)
    compressed = f"{msg["Source"]}\t{msg["Destination"]}"
    if msg["Type"] == "REQUEST":
        # do something
        compressed += "\t1\t"
    elif msg["Type"] == "RESPONSE":
        compressed += "\t2\t"
    else:
        compressed += f",{msg["Type"]},"

    compressed += msg["Payload"]
    return compressed
    # 1,3,R,payload


def decompress_mes(m):
    msg = m.split("\t")
    src = msg[0]
    dest = msg[1]
    type = msg[2]
    payload = msg[3]
    
    if type == '1':
        type = "REQUEST"
    elif type == '2':
        type = "RESPONSE"

    return create_message(src,dest,type,payload)


from lib.Act02Chapter01 import Act02Chapter01
# Welcome to the StellarScript Console!

def client_strategy(self):

    self.set_display(f"Queued:{len(self.from_layer_3())}")

    # Handle messages received from our interfaces, destined for layer 3 
    while(self.message_queue):
        m = self.message_queue.pop()
        m.text = decompress_mes(m.text)
        # If this message belongs to us, send it to layer 3
        if parse_message(m.text).get("Destination") == self.id:
            self.to_layer_3(m.text)
                
    # Handle messages from layer 3, destined for the interfaces
    # Each entity has a single interface in this scenario. Figure out what the interface is.
    connected_interfaces = self.connected_interfaces() # This should return a list of exactly one value.
    selected_interface = connected_interfaces[0] 

    # find whose turn
    x = math.floor((self.current_tick()%60)/15) + 1

    # if it's not my turn, return
    if str(x) != self.id:
        return

    ticks_remaining = 15 - self.current_tick()%15

    # if it's not sending and can send the whole thing b/f the end of the turn, send
    if not self.interface_sending(selected_interface):            
        while len(self.from_layer_3()) > 0:
            msg_text = self.from_layer_3().pop()
            compressed = compress_mes(msg_text)

            padding = 0
            if self.id == '1':
                padding = 1
            elif self.id == '4':
                padding = 2
            elif self.id == '3':
                padding = -6

            # if the entire message can be sent b/f the end of the turn
            if (math.ceil(len(compressed)/50) + padding) < ticks_remaining:
                self.send_message(compressed, selected_interface)
                break




########################
## Run the simulation ##
########################
s = Act02Chapter01(
            client_strategy,
            client_strategy,
             animation_frames=3,
             wait_for_user=False

            )
s.run()

```

**2. Explain your strategies in English.**

- We tried a couple different things to try to get this chapter to have a better success rate. First, we tried to use ALOHA and slotted ALOHA, but these did not prove to be very successful with our implementation of them.
- The more successful solution for us was using time division multiplexing with a couple additions to make it more efficient. 
    - Firstly, we compressed the messages a little bit to make them smaller which would ideally mean they take less time to send. 
    - The other addition that we made was to calculate how many ticks it would take for a message to be sent and make it past the intersection where interference occurs and to only send a message if it would be sure to be fully sent and make it to the intersection on this turn. This means that if a message is too long to send this turn without potential interference, it would not be sent. This makes it so that there is zero interference.
    - Then we messed around with the amount of padding (extra time that a message needed on a turn in order to not collide with messages from other sources) to try to make it waste as little time as possible. This solution would give a success rate somewhere between 0.25 and 0.285 over 300 ticks depending on which time I ran it.

**3. What was your maximum steady-state success rate (after 300 or so ticks?)**
- The maximum success rate that we saw after about 300 ticks was 0.285.

**4. Evaluate your strategy. Is it good? Why or why not?**
- The strategy is good, although I think it could be a little bit better. It takes advantage of most of the bandwidth meaning that there is a message being transmitted most of the time. It also has zero collisions which helps a lot for making the success rate go up.

**5. Are there any strategies you'd like to implement, but you don't know how?**
- None that come to mind

**6. Any other comments?**
- I would recommend that there is either a way to view the top message on the `self.from_layer_3()` without using the `pop()` method, or to have a `push()` method that can do the reverse of `pop()`. The reason for this is that I pop the message to send it, but if it is too long for me to send on my current turn, I cannot put it back on the list. This means that a lot of the messages just go to waste. If I could view the message without popping it, or if I could put it back if I don't send it, I think it would be better. This is because for my strategy, my queues of messages will sometimes run out of messages to send because they are thrown away, so there is some blank time that is being unused.