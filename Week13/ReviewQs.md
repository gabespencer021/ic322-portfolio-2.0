# Review Questions

# Chapter 6

## R10 Suppose nodes A, B, and C each attach to the same broadcast LAN (through their adapters). If A sends thousands of IP datagrams to B with each encapsulating frame addressed to the MAC address of B, will C’s adapter process these frames? If so, will C’s adapter pass the IP datagrams in these frames to the network layer C? How would your answers change if A sends frames with the MAC broadcast address?
Because it is a broadcast LAN, C's adapter will process all of the frames that A sends. This is because every node will process every frame to see to whom it is addressed. However, C's adapter will not pass the IP datagrams in these frames to the network layer C because they are addressed to B. If C processes anything that is not addressed to it, B will drop that frame.

If A sends frames with the MAC broadcast address, then when C processes the frames, it will see that they are addressed to C (because a broadcast address means it is addressed to every node). When C sees the broadcast address, it will pass the IP datagrams up to the network layer C.

## R11 Why is an ARP query sent within a broadcast frame? Why is an ARP response sent within a frame with a specific destination MAC address?
A ARP query is sent within a broadcast frame because an ARP query means the destination MAC address is unknown, so it must broadcast to every MAC address to make sure that the intended target actually receives the frame. The ARP response frame is sent with a specific destination MAC address because the ARP query contains a source MAC address, so the receiver of the frame knows the MAC address of the destination for where its response must go.

## P14 Consider three LANs interconnected by two routers, as shown in Figure 6.33.
![Figure 6.33](Figure6.33.png)

### a. Assign IP addresses to all of the interfaces. For Subnet 1 use addresses of the form 192.168.1.xxx; for Subnet 2 uses addresses of the form 192.168.2.xxx; and for Subnet 3 use addresses of the form 192.168.3.xxx.
**A.**: 192.168.1.001
**B.**: 192.168.1.002
**C.**: 192.168.2.001
**D.**: 192.168.2.002
**E.**: 192.168.3.001
**F.**: 192.168.3.002

### b. Assign MAC addresses to all of the adapters.
**A**: A0-00-00-00-00-00
**B**: B0-00-00-00-00-00
**C**: C0-00-00-00-00-00
**D**: D0-00-00-00-00-00
**E**: E0-00-00-00-00-00
**F**: F0-00-00-00-00-00
**R1**: 10-00-00-00-00-00
**R2**: 20-00-00-00-00-00

### c. Consider sending an IP datagram from Host E to Host B. Suppose all of the ARP tables are up to date. Enumerate all the steps, as done for the single-router example in Section 6.4.1.
1. E creates an IP datagram destined for B.
2. E encapsulates the IP datagram in a frame with R2's MAC address as its destination.
3. The frame is forwarded to R2 by the switch, and R2 removes the IP datagram.
4. R2 sees the destination IP address of B and re-encapsulates the IP datagram in a frame with a destination MAC address of R1.
5. The frame is forwarded to R1 by the switch, and R1 removes the IP datagram.
6. R2 sees that the destination IP address is B and it re-encapsulates the frame now with B's MAC address as its destination.
7. The frame is forwarded to B by the switch and B removes the IP datagram and processes the message.

### d. Repeat (c), now assuming that the ARP table in the sending host is empty (and the other tables are up to date).
1. E creates an IP datagram destined for B.
2. E sends an ARP query for the MAC of R2.
3. The switch broadcasts the ARP query and updates is ARP table with E's MAC address
4. R2 sends an ARP response to E.
5. The switch updates its ARP table with R2's MAC address and forwards the response to E.
6. E updates its ARP table with the MAC address of R2.
7. E encapsulates the IP datagram for B in a frame with R2's MAC address as its destination and sends it.
8. The switch sends the frame to R2.
9. R2 receives the frame and processes it, finds that it needs to send to B, so it sends an ARP query for the MAC of R2 (the next hop towards B)
10. The switch in subnet 2 broadcasts the ARP query and updates its ARP table with R2's MAC address.
11. R1 receives the query and makes an ARP response and sends it.
12. The switch in subnet 2 sends the response to R2 and updates its ARP table with R1's MAC address.
13. R2 receives the response and updates its ARP table with R1's MAC address.
14. R2 encapsulates the IP datagram for B in a frame with destination address of R1's MAC address and sends it.
15. The switch in subnet 2 forwards the frame to R1.
16. R1 receives the frame and processes it, sees that it has to go to B, so it sends an ARP query for B's MAC address.
17. The switch in subnet 1 broadcasts the ARP query to everyone on the subnet and updates its ARP table with R1's MAC address.
18. B receives the query and responds.
19. The switch in subnet 1 forwards the ARP response to R1 and updates its ARP table with B's MAC address.
20. R1 receives the ARP response, updates its ARP table with B's MAC address, and encapsulates the IP datagram for B in a frame with B's MAC address as its destination.
21. The switch in subnet 1 forwards the frame to B.
22. B receives the frame and processes it.

## P15 Consider Figure 6.33. Now we replace the router between subnets 1 and 2 with a switch S1, and label the router between subnets 2 and 3 as R1.

### a. Consider sending an IP datagram from Host E to Host F. Will Host E ask router R1 to help forward the datagram? Why? In the Ethernet frame containing the IP datagram, what are the source and destination IP and MAC addresses?
- E will not ask R1 to help forward the datagram because E and F are both in subnet 3.
- Source IP: 192.168.3.001
- Destination IP: 192.168.3.002
- Source MAC: E0-00-00-00-00-00
- Destination MAC: F0-00-00-00-00-00

### b. Suppose E would like to send an IP datagram to B, and assume that E’s ARP cache does not contain B’s MAC address. Will E perform an ARP query to find B’s MAC address? Why? In the Ethernet frame (containing the IP datagram destined to B) that is delivered to router R1, what are the source and destination IP and MAC addresses?
- E will not perform an ARP query to find B's MAC address because B and E are not in the same subnet. E will perform an ARP query to find R1's MAC address.
- Source IP: 192.168.3.001
- Destination IP: 192.168.1.002
- Source MAC: E0-00-00-00-00-00
- Destination MAC: 10-00-00-00-00-00
    - supposed to be MAC address of R1, but it's confusing which one this is becasue R1 got switched.

### c. Suppose Host A would like to send an IP datagram to Host B, and neither A’s ARP cache contains B’s MAC address nor does B’s ARP cache contain A’s MAC address. Further suppose that the switch S1’s forwarding table contains entries for Host B and router R1 only. Thus, A will broadcast an ARP request message. What actions will switch S1 perform once it receives the ARP request message? Will router R1 also receive this ARP request message? If so, will R1 forward the message to Subnet 3? Once Host B receives this ARP request message, it will send back to Host A an ARP response message. But will it send an ARP query message to ask for A’s MAC address? Why? What will switch S1 do once it receives an ARP response message from Host B?
S1 will first update its ARP table with A's MAC address (the source of the ARP request). Then it will broadcast the ARP request message (meaning that it will send it to all its connected interfaces except for the one that it received the request from). R1 will receive this ARP request message because it was broadcast. But R1 will not forward the message to subnet 3. 

B will not send an ARP request message for A's MAC address because A's MAC address was the source address of the ARP request that B received, so B already knows A's MAC address. S1 will forward the ARP response message to A only because the frame has that as its destination and S1 has that MAC address in its ARP table.


# Chapter 7

## R3 What are the differences between the following types of wireless channel impairments: path loss, multipath propagation, interference from other sources?
***Path Loss***: As the distance between sender and receiver increases, the signal will disperse, causing a decreased signal strength. Signal will also be inhibited when it has to pass through matter, such as a wall.
***Multipath Propagation***: This occurs when parts of the waves that make up a signal relect off of different objects causing them to take different paths between the sender and receiver. This causes the received signal to be blurred.
***Interference from Other Sources***: Different sources that transmit at the same frequency at the same time will cause interference. This can be anything from radio sources to nearby motors or microwaves.

## R4 As a mobile node gets farther and farther away from a base station, what are two actions that a base station could take to ensure that the loss probability of a transmitted frame does not increase?
The base station can increase the power of its transmission, or it can decrease the transmission rate.

## P6 In step 4 of the CSMA/CA protocol, a station that successfully transmits a frame begins the CSMA/CA protocol for a second frame at step 2, rather than at step 1. What rationale might the designers of CSMA/CA have had in mind by having such a station not transmit the second frame immediately (if the channel is sensed idle)?
I the station transmits right away after it successfully transmits one frame, it would never allow other stations to transmit as long as it had messages, so it would not share the bandwidth.

## P7  Suppose an 802.11b station is configured to always reserve the channel with the RTS/CTS sequence. Suppose this station suddenly wants to transmit 1,500 bytes of data, and all other stations are idle at this time. As a function of SIFS and DIFS, and ignoring propagation delay and assuming no bit errors, calculate the time required to transmit the frame and receive the acknowledgment.
- $t_{rts}$ = time required to transmit RTS
- $t_{cts}$ = time required to transmit CTS
- $t_{data}$ = time required to transmit the 1500 bytes of data
- $t_{ack}$ = time required to transmit ACK

$$t_{total} = DIFS + t_{rts} + SIFS + t_{cts} + SIFS + t_{data} + SIFS + t_{ack}$$