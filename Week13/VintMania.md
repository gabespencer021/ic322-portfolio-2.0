# VINT MANIA

## Vint Cerf is known as "one of the fathers of the Internet". Why is he known as this?
Vint Cerf is one of the developers of the TCP/IP protocol. He also worked on the development of the ARPANET, the precursor to the internet. 

## Find 3 surprising Vint Cerf facts.
1. While in high school, he worked on the Apollo program at Rocketdyne.
2. He received a bachelor's degree in mathematics from Stanford.
3. He is the co-inventor of Knowbot programs, which are mobile software agents in the network environment.

## Develop 2 interesting and insightful questions you'd like to ask him. For each question, describe why it is interesting and insightful.
1. Did you have a clear goal when developing the TCP/IP protocol, or did you just see how you could tweak the product piece by piece?
    - This is interesting and insightful because the answer would shed some light on whether the invention was a great solution to a certain issue that was there, or whether it was a solution to a problem that wasn't fully realized yet which would make it even more impactful.
2. What were your goals for the ARPANET while developing it, and did you foresee the modern day internet coming from it?
    - This is interesting and insightful because the answer would give some insight into what some of the original ideas for the internet were and how they have or have not been realized since then.