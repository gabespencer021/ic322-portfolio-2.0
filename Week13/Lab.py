import json
import math

def create_message(src, dest, msg_type, payload):
    """If `payload` is a string, it will come back out as a string. If it's a
    Dict, it will come back out as a dict."""
    
    m = {
        "Source": src,
        "Destination": dest,
        "Type": msg_type,
        "Payload": payload
    }

    return json.dumps(m)

def parse_message(m):
    parsed = json.loads(m)
    return {
        "Source": parsed["Source"],
        "Destination": parsed["Destination"],
        "Type": parsed["Type"],
        "Payload": parsed["Payload"]
    }

def compress_mes(m):
    msg = parse_message(m)
    compressed = f"{msg["Source"]}\t{msg["Destination"]}"
    if msg["Type"] == "REQUEST":
        # do something
        compressed += "\t1\t"
    elif msg["Type"] == "RESPONSE":
        compressed += "\t2\t"
    else:
        compressed += f",{msg["Type"]},"

    compressed += msg["Payload"]
    return compressed
    # 1,3,R,payload


def decompress_mes(m):
    msg = m.split("\t")
    src = msg[0]
    dest = msg[1]
    type = msg[2]
    payload = msg[3]
    
    if type == '1':
        type = "REQUEST"
    elif type == '2':
        type = "RESPONSE"

    return create_message(src,dest,type,payload)


from lib.Act02Chapter01 import Act02Chapter01
# Welcome to the StellarScript Console!

def client_strategy(self):

    self.set_display(f"Queued:{len(self.from_layer_3())}")

    # Handle messages received from our interfaces, destined for layer 3 
    while(self.message_queue):
        m = self.message_queue.pop()
        m.text = decompress_mes(m.text)
        # If this message belongs to us, send it to layer 3
        if parse_message(m.text).get("Destination") == self.id:
            self.to_layer_3(m.text)
                
    # Handle messages from layer 3, destined for the interfaces
    # Each entity has a single interface in this scenario. Figure out what the interface is.
    connected_interfaces = self.connected_interfaces() # This should return a list of exactly one value.
    selected_interface = connected_interfaces[0] 

    # find whose turn
    x = math.floor((self.current_tick()%60)/15) + 1

    # if it's not my turn, return
    if str(x) != self.id:
        return

    ticks_remaining = 15 - self.current_tick()%15

    # if it's not sending and can send the whole thing b/f the end of the turn, send
    if not self.interface_sending(selected_interface):            
        while len(self.from_layer_3()) > 0:
            msg_text = self.from_layer_3().pop()
            compressed = compress_mes(msg_text)

            padding = 0
            if self.id == '1':
                padding = 1
            elif self.id == '4':
                padding = 2
            elif self.id == '3':
                padding = -6

            # if the entire message can be sent b/f the end of the turn
            if (math.ceil(len(compressed)/50) + padding) < ticks_remaining:
                self.send_message(compressed, selected_interface)
                break




########################
## Run the simulation ##
########################
s = Act02Chapter01(
            client_strategy,
            client_strategy,
             animation_frames=3,
             wait_for_user=False

            )
s.run()