# Week 13: More Link Layer

*Gabriel Spencer | Fall 2023*

This week we learned about switches, ARP, and LANs, as well as CSMA/CA and how it differs from CSMA/CD.

* [Review Questions](Week13/ReviewQs.md)
* [Learning Goals](Week13/LearningGoals.md)
* [Lab: Beta Tester](Week13/Lab.md)
* [Vint Mania](Week13/VintMania.md)
* [Community Garden: Feedback](https://gitlab.usna.edu/kvea25/ic322-portfolio/-/issues/19)
* [Back to Overview](../README.md)
