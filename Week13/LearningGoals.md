# Learning Goals

## I can describe the role a switch plays in a computer network.
A switch works within the link layer on a subnet. It is responsible for routing frames within a subnet, making sure the right frames go to the right places. It does this via **forwarding** and **filtering**. If a switch receives too many frames to send at one time, it will buffer them so that there are no collisions.

### Forwarding:
- Forwarding is when a switch receives frames and sends them along to their destination along the correct interface.
- The switch knows which interface to send the frame on because of its **Switch Table**
- If a switch receives a frame with a destination MAC address that it does not recognize in its switch table, it will broadcast the frame to the whole subnet so that the intended target will be sure to receive it.
- When forwarding, switches act very much like routers, except instead of using IP addresses, they use MAC addresses.
### Filtering:
- Filtering is when a switch decides whether or not to drop a frame.

### Switch Tables:
- contain three pieces of information for each entry:
    1. MAC address
    2. Interface for that address
    3. Time that it got this information
- This allows the switch to know where to send frames on for which destination.
- The time value allows the switch table to make sure that it is not keeping old values that are no longer useful. Entries in the table will expire after some time so that when hosts and disconnected and reconnected to a subnet, the switch has no trouble knowing which MAC address is where.

### 3 Cases:
To understand how switch filtering and forwarding work, there are three possible cases. Consider the case that a frame with destination address *DD-DD-DD-DD-DD-DD* arrives at the switch on interface *x*.
1. There is no table entry for *DD-DD-DD-DD-DD-DD*:
    - In this case, the switch forwards copies of the frame to the output buffers on *all* interfaces except *x*. In other words, if there is no table entry for the destination address, the switch broadcasts the frame.
2. There is an entry for *DD-DD-DD-DD-DD-DD* associating it with interface *x*:
    - In this case, the frame si coming from a LAN segment that contains adapter *DD-DD-DD-DD-DD-DD*, so there is no need to forward the frame to any other interface. The switch performs the filtering function by discarding the frame.
3. There is an entry in the table for *DD-DD-DD-DD-DD-DD* associating it with a an interface *y* $\neq$ *x*:
    - In this case, the frame needs to be forwarded to the LAN segment attached to the interface *y*. The switch performs forwarding by putting the frame in an output buffer that precedes interface *y*.

### Different from Hubs:
Another link layer device that can be used is called a hub. Switches and hubs are similar, but a hub just broadcasts everything. A switch is better than a hub for a few reasons.
1. **Elimination of Collisions**: When a LAN is built with switches and no hubs, there are no collisions because the switches will buffer frames and will not transmit more than one frame on a given segment at any time.
2. **Heterogeneous Links**: Because a switch isolates links on the same LAN from each other, each different link can run at a different speed and over different media without issues.
3. **Management**: Switches make it very easy to manage a network. If an adapter malfunctions or a cable disconnects, only the host that was using that adapter or cable is affected. The switch can realize that a certain interface is causing issues and can disconnect that interface. This makes it easier on the network administrator because it keeps issues local so the entire network can keep running when there are local issues. Switches can also collect data for statictics on bandwidth usage, collision rates, and traffic types, and can make this information available to the network manager. This provides very useful information for future development of LANs to make them better.

## I can explain the problem ARP solves, how it solves the problem, and can simulate an ARP table given a simple LAN scenario.
ARP solves the problem of having different addresses for different layers (IP addresses for the network layer and MAC addresses for the link layer). It solves this by making a table that maps IP addresses to MAC addresses. Mapping IP addresses to MAC addresses is basically analagous to how DNS maps hostnames to IP addresses. The biggest difference is that DNS can work to get IP addresses from anywhere on the internet while ARP only translates between IP and MAC addresses within the same subnet. Each host and router on a subnet has an ARP table so that it can translate between IP and MAC addresses when trying to forward datagrams and frames. Each ARP table entry has three values:
1. IP address
2. MAC address
3. TTL
    - The TTL (time to live) value is the time that the entry in the table will expire. It typically lasts 20 minutes from when the table entry is created.

### Example:
![Simple LAN Scenario](arpex.png)

| IP Address | MAC Address | TTL |
| :--------: | :---------: | :-: |
| 222.222.222.220 | 1A-23-F9-CD-06-9B | 12:24:00 |
| 222.222.222.221 | 88-B2-2F-54-1A-0F | 13:45:00 |
| 222.222.222.222 | 5C-66-AB-90-75-0F | 13:52:00 |
| 222.222.222.223 | 49-BD-D2-C7-56-2A | 14:42:00 |

In this example, each IP address has a MAC address and each of those entries has a TTL field. This way, each router and host that has these tables can know which MAC address to send to based on what the IP address destination is for the datagram.

## I can explain CSMA/CA, how it differs from CSMA/CD, what problems it addresses, and how it solves them.
CSMA/CA stands for Carrier Sensing Multiple Access with Collision Avoidance. It is the random access protocol that the 802.11 MAC protocol uses to send frames. The goal of CSMA/CA is to get the largest throughput with the fewest collisions possible. It is similar to CSMA/CD in the fact that it will listen before it sends and will only send if no one else is sending. It uses collision avoidance instead of collision detection because collisions can be very difficult to detect. This is because the incoming signal can be very weak compared to the sending one, and some hardware doesn't even support sending and receiving at the same time. Another reason is if the router is within range of two hosts that are not within range of each other, they could be colliding at the router, but they would have no idea because they cannot detect the collisions because they have no ability to see the signal from the other host. Because it cannot reliably detect collisions, for collision avoidance to work, it has to use acknowledgments. For each frame sent, there has to be an acknowledgment received by the sender of the frame in order to know that it was received properly.

CSMA/CA has four steps:
1. If it senses an idle channel initially, it will transmit its frame after waiting a short period of time known as the *Distributed Inter-frame Space* (DIFS).
2. If the channel is busy, it will choose a random backoff time using binary exponential backoff. It will count down this value after DIFS once the channel is sensed to be idle. When the channel is busy, the counter value does not change.
3. When the counter reaches zero, the station transmits the frame and waits for an acknowledgment.
4. If an acknowledgment is received, the transmitting station knows that its frame has been correctly received at the destination. If it has another frame to send, it will begin again at step 2. If the acknowledgment is not received, the transmitting station goes back to the backoff phase in step 2, but with the random value being chosen from a larger interval.