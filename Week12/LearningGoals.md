# Learning Goals

## I can explain the ALOHA and CSMA/CD protocols and how they solve the multiple-access problem.
The multiple access problem:
- Multiple nodes communicate on the same channel
- When they send information at the same time, it is not readable

**ALOHA:**
- **Slotted ALOHA**:
    - The time is divided into segments that are the amount of time it takes to send one frame
    - If a node has a message, it will begin sending it at the beginning of a slot
    - If there is a collision detected, the node will try again to resend the frame in the next slot
    - After a collision, a frame sends during the next slot with probability *p*
    - If it is not sent during a given slot, then it sends with probability *p* in the next slot
- **Unslotted ALOHA**:
    - The nodes are not synchronized
    - They send whenever they receive a message
    - If a collision is detected, there is probability *p* that they resend immediately
    - If it does not send, it will wait enough time for one frame to send, then it will once again use *p* to decide if it will send again
- Slotted ALOHA is more efficient than unslotted

**CSMA/CD**
- Carrier Sensing Multiple Access with Collision Detection
- Carrier Sensing
    - This means that each node listens to whether or not another node is transmitting
    - If another node is broadcasting, it will not send its own message out
- A node will only send once there are no other nodes transmitting
- If it detects a collision, then it will immediately stop transmitting
- It will start listening again after a random amount of time passes
- Despite the fact that the nodes listen, collisions still occur because of propagation delay because a node will transmit, and before its signal reaches the other node, it too will begin transmitting because it can't hear anything.

## I can compare and contrast various error correction and detection schemes, including parity bits, 2D parity, the Internet Checksum, and CRC.
**Parity Bits**

A parity bit is a bit that identifies whether the total number of bits that are 1s in the data should be even or odd. A parity bit of 1 means that there is an even number of 1s and 0 means there is an odd number. This means that if the number of 1s is even and the parity bit is 1, there is an error. Likewise, if the number of 1s is odd and the parity bit is 0, there is an error. This will detect any odd number of bit flips, but not even numbers of bit flips. For this reason, while it is very simple and easy to implement, it is not reliable.

**2D Parity**

2D parity is when the data is divided into equal parts, and they are put in a grid. Then each row and column receives its own parity bit with the same assignment rules as above. There is also one more parity bit for the whole thing. If there is an error of a single bit flip, using both the column and row parity bits, you can pinpoint where the error is and correct it. When there are more than 1 flipped bits, you cannot pinpoint where the error is, but only that there is an error. This method, like regular parity bits, still has certain combinations of bit flips that will go undetected. This method is also simple to implement, but not super reliable.

**Internet Checksum**
The Internet Checksum is when the data is broken up into portions that are each 16 bits long and each portion is treated as a 16 bit integer, and they are added to each other. Then take the ones complement of the solution, and that number is the checksum which is sent along with the data in the header of the segment. The receiver takes the ones complement of the sum of the data that it received (including the checksum), and checks if the results are all 0s. If there are any ones in the result, then there was an error. This requires very little overhead for the frames, and it is a simple computation making it easy to implement.

**CRC**
CRC stands for Cyclic Redundancy Check. It is a method in which CRC codes are treated as polynomials and the ones and zeroes are treated as the coefficients. Firstly, there must be a generator *G* that is agreed on by both the sender and receiver in which the leftmost bit must be a one. For some data *D* of length *d* bits that the sender wants to send to the receiving node, the sender must find *R* which is of *r* length and is the remainder when $D\times2^r$ is divided by *G*. This remainder *R* is then sent as the CRC bits to verify the frame at the receiving node. The receiving node adds *R* to $D\times2^r$ and then divides the answer by *G*. If there are no errors in the message, the remainder should be zero, and if there is a remainder, then the receiver knows that there was an error in the message.

## I can describe the Ethernet protocol including how it implements each of the Layer 2 services and how different versions of Ethernet differ.
Ethernet is a method of wired communication on a LAN and is by far the most popular version of LAN communication. It operates on switches which uses store-and-foreward for frames which eliminates collisions that would happen with hubs. It is different from routers because a router uses layer 3 and a switch works on layer 2. The structure of an Ethernet frame (pictured below) can teach a lot about Ethernet.

![Ethernet Frame](image-5.png)

It has six fields: the preamble, source and destination addresses, type, data, and CRC.
- *Preamble*:
    - an 8-byte field
    - The first seven bytes all contain the following bits: 10101010
    - The last byte contains 10101011
    - These are to prep the receiver and synchronize it because Ethernet messages can be sent at different rates
- *Destination Address*:
    - This 6-byte field contains the destination MAC address of the adapter. 
    - When the adapter receives a frame with either its own address or the broadcast address, it passes the contents of the data field to layer 3.
    - If it receives any other MAC address, it discards the packet.
- *Source Address*:
    - This 6-byte field contains the MAC address of the adapter that transmits the frame onto the LAN.
- *Type Field*:
    - This 2-byte field enable Ethernet to multiplex between network-layer protocols.
    - There are multiple layer 3 protocols that need to receive the right frames. The type field exists to make sure that each frame makes it to the correct protocol on layer 3.
- *Data Field*:
    - Anywhere from 46 to 1500 bytes
    - This carries the IP datagram
    - If the IP datagram is longer than 1500 bytes, it must be fragmented
    - If the IP datagram is shorter than 46 bytes, it will be "stuffed"
        - In the case that the data field is "stuffed", the entire datagram with the "stuffing" will be passed up to the network layer.
        - The network layer uses the length field in the IP datagram header to remove the "stuffing".
- *CRC*:
    - A 4-byte field that allows the receiver to detect errors in the message.
    - It is calculated by the CRC method explained above