# Protocol Pioneer Chapter 5

## Introduction

This lab was setting up a strategy to get the best success rate for sending and receiving transmission between routers when considering interference.

## Collaboration

I collaborated with Alex Traynor for this lab.

## Process

I followed the instructions on the [course website](https://ic322.com/#/assignments/week12/protocol-pioneer/) for this lab.

## Questions

1. How did you solve the Chapters? Please copy and paste your winning strategy(s), and also explain it in English.
The strategy was to give each client and server an equal amount of time during which they would transmit messages, and if it was not their turn to transmit, the would just listen. This would limit the interference because there would never be two messages being sent at the same time. After some testing to find the optimal number of ticks per turn, we found that 16 ticks for each client and server for each turn would give the best results. The fancy term for our strategy is time division multiplexing. Our success rate got up to about 0.2 successes/tick.

```
import json

def create_message(src, dest, msg_type, payload):
    """If `payload` is a string, it will come back out as a string. If it's a
    Dict, it will come back out as a dict."""
    
    m = {
        "Source": src,
        "Destination": dest,
        "Type": msg_type,
        "Payload": payload
    }

    return json.dumps(m)

def parse_message(m):
    parsed = json.loads(m)
    return {
        "Source": parsed["Source"],
        "Destination": parsed["Destination"],
        "Type": parsed["Type"],
        "Payload": parsed["Payload"]
    }


from lib.Act02Chapter01 import Act02Chapter01
import math
# Welcome to the StellarScript Console!

def server_strategy(self):
    
    self.set_display(f"Queued:{self.id}")

    # Handle messages received from our interfaces, destined for layer 3 
    while(self.message_queue):
            m = self.message_queue.pop()
            print(f"--- Server {self.id}: Msg on interface {m.interface}: Tick {self.world.get_current_tick()} ---\n{m.text}\n------------------")
            
            # If this message belongs to us, send it to layer 3
            if parse_message(m.text).get("Destination") == self.id:
                self.to_layer_3(m.text)


    # Handle messages from layer 3, destined for the interfaces
    # Each entity has a single interface in this scenario. Figure out what the interface is.
    connected_interfaces = self.connected_interfaces() # This should return a list of exactly one value.
    selected_interface = connected_interfaces[0] 
    x = math.floor((self.current_tick()%64)/16) + 1
    if not self.interface_sending(selected_interface):
        if str(x) == self.id:
            if len(self.from_layer_3()) > 0:
                msg_text = self.from_layer_3().pop()
                print(f"{self.id}: Attempting send.")
                self.send_message(msg_text, selected_interface)

def client_strategy(self):
    
    self.set_display(f"Queued:{len(self.from_layer_3())}")

    # Handle messages received from our interfaces, destined for layer 3 
    while(self.message_queue):
            m = self.message_queue.pop()
            print(f"--- Client {self.id}: Msg on interface {m.interface}: Tick {self.world.get_current_tick()} ---\n{m.text}\n------------------")
            
            # If this message belongs to us, send it to layer 3
            if parse_message(m.text).get("Destination") == self.id:
                self.to_layer_3(m.text)


    # Handle messages from layer 3, destined for the interfaces
    # Each entity has a single interface in this scenario. Figure out what the interface is.
    connected_interfaces = self.connected_interfaces() # This should return a list of exactly one value.
    selected_interface = connected_interfaces[0] 
    x = math.floor((self.current_tick()%64)/16) + 1
    # print(x)
    if not self.interface_sending(selected_interface):            
        if str(x) == self.id:      
            if len(self.from_layer_3()) > 0:
                msg_text = self.from_layer_3().pop()
                print(f"{self.id}: Attempting send.")
                self.send_message(msg_text, selected_interface)




########################
## Run the simulation ##
########################
s = Act02Chapter01(
            client_strategy,
            server_strategy,
             animation_frames=2,
             wait_for_user=False

            )
s.run()
```

2. Include a section on Beta Testing Notes.

- List any bugs you find. When you list the bugs, try to specify the exact conditions that causes them so I can reproduce them.
    - I did not find any bugs.
- Was there anything that you and others were consistently confused about? What would you do to solve that confusion?
    - I did not find anything confusing
- Was there too much narrative? Not enough narrative?
    - Good amount of narrative
- Was it fun?
    - It was especially fun when we would test each time that we edited our strategy, and when the success rate would go higher, it was exciting.
- Really, just let me know how I can improve the game.
    - No particular recommendations