# Week 12: Link Layer

*Gabriel Spencer | Fall 2023*

This week we learned about ALOHA and CSMA/CD as well as how the link layer provides error checking.

* [Review Questions](Week12/ReviewQs.md)
* [Learning Goals](Week12/LearningGoals.md)
* [Lab: Beta Tester](Week12/Lab.md)
* [Community Garden: Feedback](https://gitlab.com/raintree06/ic322-portfolio/-/issues/23)
* [Back to Overview](../README.md)
