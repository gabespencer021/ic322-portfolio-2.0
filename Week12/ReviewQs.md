# Review Questions

## R4. Suppose two nodes start to transmit at the same time a packet of length *L* over a broadcast channel of rate *R*. Denote the propagation delay between the two nodes as *d<sub>prop</sub>*. Will there be a collision if *d<sub>prop</sub>* < *L/R*? Why or why not?
If *d<sub>prop</sub>* < *L/R*, then there will be a collision. *d<sub>prop</sub>* is the propagation delay, and *L/R* is the transmission delay. If the propagation delay is less than the transmission delay, then the information will start arriving at a node while the other node is still sending information, causing a collision.

## R5. In Section 6.3, we listed four desirable characteristics of a broadcast channel. Which of these characteristics does slotted ALOHA have? Which of these characteristics does token passing have?
The four desirable characteristics are the following:
1. When only one node has data to send, that node has a throughput of R bps.
2. When M nodes have data to send, each of these nodes has a throughput of R/M bps. This need not necessarily imply that each of the M nodes always has an instantaneous rate of R/M, but rather that each node should have an average transmission rate of R/M over some suitably defined interval of time.
3. The protocol is decentralized; that is, there is no master node that represents a single point of failure for the network.
4. The protocol is simple, so that it is inexpensive to implement.

ALOHA:
1. When it is sending its data and no other node has anything to send, then there will be no collisions, so it will have a throughput of R bps.
2. When M nodes have data to send, then the successful throughput of a 100 Mbps channel will only be 37 Mbps, so it will not quite achieve R/M.
3. It is decentralized because the nodes just run one after the other, so if any particular one fails, the other ones will continue functioning just fine.
4. It is very simple.

Token passing:
1. If it has lots of things to send, it will send up to the max number, then pass the token. It will not quite be throughput of R bps, but it will be highly efficient.
2. It will be highly efficient, making the average throughput of each node R/M.
3. It is decentralized because it does not have a master node, but if one node fails, then once it has the token, the whole system fails because the other nodes cannot send anything without the token.
4. It is relatively simple... not a lot of overhead. Just one token is necessary.

## R6. In CSMA/CD, after the fifth collision, what is the probability that a node chooses K=4? The result K=4 corresponds to a delay of how many seconds on a 10 Mbps Ethernet?
After 5 collisions, the number will be randomly chosen from {0,1,2,3,...,30,31}, and so the probability that K=4 is 1/32 or 0.03125. K-4 would result in a delay of K * 512, so 4 * 512 = 2048 bits times. This would be 204.8 microseconds on a 10 Mbps Ethernet.

## P1. Suppose the information content of a packet is the bit pattern 1110 0110 1001 0101 and an even parity scheme is being used. What would the value of the field containing the parity bits be for the case of a two-dimensional parity scheme? Your answer should be such that a minimum-length checksum field is used.
```
1 1 1 0 | 1
0 1 1 0 | 0
1 0 0 1 | 0
0 1 0 1 | 0
--------|--
0 1 0 0 | 0
```

## P3. Suppose the information portion of a packet (D in Figure 6.3) contains 10 bytes consisting of the 8-bit unsigned binary ASCII representation of string “Internet.” Compute the Internet checksum for this data.
```
    01001001 01101110
  + 01110100 01100101
  -------------------
    10111101 11010011
  + 01110010 01101110
  -------------------
   100110000 01000001
     - Overflow, the 1 from the left gets added on the right
    00110000 01000010
  + 01100101 01110100
  -------------------
    10010101 10110110

    internet checksum: 01101010 01001001
     - one's complement of sum
```

## P6. Consider the generator, G = 10011, and suppose that D has the values listed below. What is the value of R?
**a.** 1000100101
- 1000100101000 divided by 10011, and the remainder is R
- R = 1000
**b.** 0101101010
- 0101101010000 divided by 10011, and the remainder is R
- R = 1110
**c.** 0110100011
- 0110100011000 divided by 10011, and the remainder is R
- R = 1010

## P11. Suppose four active nodes—nodes A, B, C and D—are competing for access to a channel using slotted ALOHA. Assume each node has an infinite number of packets to send. Each node attempts to transmit in each slot with probability p. The first slot is numbered slot 1, the second slot is numbered slot 2, and so on.
**a.** What is the probability that node A succeeds for the first time in slot 4?
- $(1-p)^3 * p * (1-p)^3$

**b.** What is the probability that some node (either A, B, C or D) succeeds in slot 5?
- $4p(1-p)^3$

**c.** What is the probability that the first success occurs in slot 4?
- $3 * (1-4p(1-p)^3) * 4p(1-p)^3$

**d.** What is the efficiency of this four-node system?
- $4p(1-p)^3$

## P13. Consider a broadcast channel with N nodes and a transmission rate of R bps. Suppose the broadcast channel uses polling (with an additional polling node) for multiple access. Suppose the amount of time from when a node completes transmission until the subsequent node is permitted to transmit (that is, the polling delay) is d<sub>poll</sub>. Suppose that within a polling round, a given node is allowed to transmit at most Q bits. What is the maximum throughput of the broadcast channel?

- Each round lasts $N(Q/R + d_{poll})$
- The maximum throughput for a given node is $Q/(Q/R + d_{poll})$
- The maximum throughput is the maximum number of bits sent ($N*Q$) in a round divided by the total time ($N(Q/R + d_{poll})$)
- This gives a final answer of $NQ / (N(Q/R + d_{poll}))$