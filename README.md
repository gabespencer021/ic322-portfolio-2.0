# IC322 Portfolio

*Gabriel Spencer | Fall AY23*

View each week's work through the link listed below: 

- [Week 1:  The Internet](Week01/README.md)

- [Week 2:  The Application Layer: HTTP](Week02/README.md)

- [Week 3:  The Application Layer: DNS and Other Protocols](Week03/README.md)

- [Week 4:  The Transport Layer: TCP and UDP](Week04/README.md)

- [Week 5:  The Transport Layer: Part II](Week05/README.md)

- [Week 7:  The Network Layer: Data Plane](Week07/README.md)

- [Week 8:  The Network Layer: More Data Plane](Week08/README.md)

- [Week 9:  The Network Layer: Control Plane](Week09/README.md)

- [Week 10: The Network Layer: Control Plane](Week10/README.md)

- [Week 12: The Link Layer](Week12/README.md)

- [Week 13: More Link Layer](Week13/README.md)

- [Week 14: Thanksgiving](Week14/README.md)

- [Week 15: More Link Layer](Week15/README.md)