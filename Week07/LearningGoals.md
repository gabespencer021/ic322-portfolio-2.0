# Learning Goals

## I can explain what a subnet is, how a subnet mask is used, and how longest prefix matching is used to route datagrams to their intended subnets.

A subnet is a group of IP addresses that have address numbers that are all within a certain range of numbers. The number of bits that are the same between all of the IP addresses on the same subnet determine what the subnet mask is. A subnet mask will show how many of its bits are part of the mask definition by adding a `/x` at the end of the IP address where *x* is the number of bits that are part of the mask (the number of bits that have to match for all IP addresses within the subnet). There are subnets within subnets because within each range of numbers that contain a bunch of IP addresses, there are smaller groups of IP addresses that match more closely, so they are within a smaller subnet.

Longest prefix matching is how routers send datagrams to the correct destination. Out of all the prefixes that match a given datagram's destination IP address, the longest prefix is chosen because that prefix is the most specific and closest to the actual host that the datagram has to reach.

## I can step though the DHCP protocol and show how it is used to assign IP addresses.

DHCP stands for Dynamic Host Configuration Protocol. It is the method that is used for hosts to get IP addresses when they connect to the network. It also provides the host with more information such as the subnet mask and the default gateway. It can assign the same IP address to the same host each time, or it can assign a new one each time a host connects. This makes it easier for a person who has a portable device so that they can take it to different places and it can connect easily. It is a four-step process when a new host is trying to connect. The four steps are below:

1. *DHCP server discovery*: This is when a new host has to find a DHCP server. It uses a *DCHP discover message* which is sent within a UDP packet to port 67. Because it does not know the destination address, it sends it to a broadcast destination of 255.255.255.255 with a source address of 0.0.0.0 so that it will go to everyone on the subnet.

2. *DHCP server offers*: A DHCP server receives the discover message and responds with a *DHCP offer message* which is again broadcast to everyone on the subnet with destination IP address of 255.255.255.255. The offer message contains the transaction ID, the proposed IP address of the client, the network mask, and an IP address lease time (how long the IP address will be valid).

3. *DHCP request*: The client will choose from one or more offer messages (it is possible to receive more than one offer) and respond with a *DHCP request message* which will echo all of the configuration parameters.

4. *DHCP ACK*: The server will respond to the *DHCP request message* with a *DHCP ACK message* that confirms the requested parameters for the IP address of the client.