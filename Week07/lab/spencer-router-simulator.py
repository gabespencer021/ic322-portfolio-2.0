# Gabriel Spencer Router Simulator
# m255964
# I sat next to Anuj and we talked about the lab to help each other

##############################################
########## Router Simulator Program ##########
##############################################

import sys

def length(myString):
    len = 0
    for x in myString:
        len += 1
    return len

def toBinary(addr):
    toReturn = ''
    binary = addr.split('.')
    for portion in binary:
        temp = bin(int(portion))
        str(temp)
        temp = str(temp.lstrip("0b"))
        while length(temp) < 8:
            temp = '0' + temp

        toReturn = toReturn + temp
        
    return toReturn

def compareIP(userInput,subnet,subnetLen):
    numLoops = 0
    for x in userInput:
        if numLoops == subnetLen:
            break
        elif x != subnet[numLoops]:
            numLoops = 0
            break
        numLoops += 1
        
    return numLoops

##### Main #####

ipInput = input()

#check that input is the right length
if length(ipInput) != 32:
    print("string of improper length input")
    exit()

routingTable = [
    ['1.0.100.0/24','Fa 0/1'],
    ['1.99.0.0/20','Fa 2/1'],
    ['8.0.0.0/27','Fa 0/2'],
    ['8.8.8.0/24','Fa 2/2'],
    ['126.2.3.0/20','Fa 0/3'],
    ['8.8.8.16/30','Fa 2/3'],
    ['237.1.1.0/30','Fa 0/4'],
    ['237.2.0.0/16','Fa 2/4'],
    ['99.31.4.0/22','Fa 0/5'],
    ['99.0.0.0/8','Fa 2/5'],
    ['101.5.0.0/16','Fa 0/6'],
    ['223.0.0.0/8','Fa 2/6'],
    ['101.5.7.32/30','Fa 0/7'],
    ['99.0.0.0/20','Fa 2/7'],
    ['101.0.0.0/8','Gi 1/0'],
    ['101.42.3.0/24','Gi 3/0'],
    ['223.4.0.0/16','Gi 1/1'],
    ['1.0.0.0/8','Fa 0/0'],
    ['1.100.56.0/24','Fa 2/0']
]
#All else Gi 3/1

bestmatch = 0
bestmatchVal = ''

for subnet in routingTable:
    addr = subnet[0].split('/')[0]
    len = int(subnet[0].split('/')[1])
    # convert to binary
    binaryIP = toBinary(addr)
    curMatch = compareIP(ipInput,binaryIP,len)
    if curMatch == 0:
        continue
    elif curMatch > bestmatch:
        bestmatch = curMatch
        bestmatchVal = subnet[1]

if bestmatch == 0:
    bestmatchVal = "Gi 3/1"
    
print(bestmatchVal)