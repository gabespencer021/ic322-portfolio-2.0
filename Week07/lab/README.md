# Running my server

The command to run the server is just `python3 spencer-router-simulator.py`. You then enter the 32 bit binary IP address into stdin and hit enter. The program will then determine what interface it should be forwarded on and print that out to stdout.