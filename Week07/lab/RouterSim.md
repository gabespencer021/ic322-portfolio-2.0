# Router Simulator Lab

## Introduction

This lab was building a basic router simulator that had a small routing table to be able to route IP address properly.

## Collaboration

I sat next to Anuj during the lab period and we helped each other a little bit with the lab.

## Process

I followed the instructions on the [course website](https://ic322.com/#/assignments/week07/lab-router-simulator/) for this lab.

## Questions

No questions for this lab.