# Review Questions

## R11. Describe how packet loss can occur at input ports. Describe how packet loss at input ports can be eliminated (without using infinite buffers).

When packets arrive at input ports, they typically have to wait to be processed and forwarded, so they wait in a buffer. This buffer is limited, so when it fills up, if there are any more packets that try to come to the same input port before it makes more space, they are dropped. You can eliminate this packet loss by making the rate that the packets are processed and forwarded faster than the rate that they arrive. This will mean that no queue will develop because the packets will be moving on faster than they build up in a queue.

## R12. Describe how packet loss can occur at output ports. Can this loss be prevented by increasing the switch fabric speed?

Packet loss can occur at output ports when the rate of the switch is faster than the port line speeds. This means that more packets will build up at the output of the switch fabric while they wait for previous packets to clear out and make room. Increasing switch fabric speed would not help this problem because it would just cause packets to arrive at the output ports more quickly, causing even more delay due to the slower port line speeds.

## R13. What is HOL blocking? Does it occur in input ports or output ports?

HOL (Head of Line) blocking is when a packet has to wait at an input port due to the fact that another packet in front of it has to wait. This means that the packet's path that it wants to take is completely free, but it still has to wait. It occurs at input ports.

## R16. What is an essential different between RR and WFQ packet scheduling? Is there a case (Hint: Consider the WFQ weights) where RR and WFQ will behave exactly the same?

The main difference between Round Robin (RR) and Weighted Fair Queuing scheduling (WFQ) is that WFQ will give more resources to classes that are assigned with a greater weight and less to classes that have a smaller assigned weight. If all of the classes are assigned the same weight, then WFQ will work the same as RR because each class will receive an equal share of the available service.

## R18. What field in the IP header can be used to ensure that a packet is forwarded through no more than *N* routers?

The `Time-to-live` field in the IP header is used to make sure that datagrams do not circulate forever. Each time the datagram is processed by a router, the field is decremented by one, so if you want to ensure that it is forwarded throught no more than *N* routers, you can set the value of the field to *N*. If a router receives a datagram with a TTL field of zero, it must drop that datagram.

## R21. Do routers have IP addresses? If so, how many?

Routers do have IP addresses. Because every host and router is capable of sending and receiving datagrams, every host and router interface must have its own IP address. This means that the IP address is not actually associated with either the router or the host, but with the interface that they contain. Because of this, a router's number of IP addresses is based on how many interfaces it has. 

## P4. Consider the switch shown below. Suppose that all datagrams have the same fixed length, that the switch operates in a slotted, synchronous manner, and that in one time slot a datagram can be transferred from an input port to an output port. The switch fabric is a crossbar so that at most one datagram can be transferred to a given output port in a time slot, but different output ports can receive datagrams from different input ports in a single time slot. What is the minimal number of time slots needed to transfer the packets shown from input ports to their output ports, assuming any input queue scheduling order you want (i.e., it need not have HOL blocking)? What is the largest number of slots needed, assuming the worst-case scheduling order you can devise, assuming that a non-empty input queue is never idle?
![diagram](Week07/p4.png)

Best Case (2 time slots):
- X and Y and Z go to their output ports (no HOL blocking)
- X and Y go to their output ports

Worst Case (3 time slots):
- X and Y go
- X and Y go
- Z goes

## P5. Suppose that the WFQ scheduling policy is applied to a buffer that supports three classes, and suppose the weights are 0.5, 0.25, and 0.25 for the three classes.

### a. Suppose that each class has a large number of packets in the buffer. In what sequence might the three classes be served in order to achieve the WFQ weights? (For round robin scheduling, a natural sequence is 123123123 . . .).

In order to achieve the WFQ weights, the classes might be served in this order: 1123112311231123. This order would give twice the time for class 1 to send packets which works because its weight is 2x that of each of the other two classes.

### b. Suppose that classes 1 and 2 have a large number of packets in the buffer, and there are no class 3 packets in the buffer. In what sequence might the three classes be served in to achieve the WFQ weights?

In order to achieve the WFQ weights, the classes might be served in this order: 112112112112. This order would give 2x the time to class 1 than to class 2 because its weight is double that of class 2.

## P8. Consider a datagram network using 32-bit host addresses. Suppose a router has four links, numbered 0 through 3, and packets are to be forwarded to the link interfaces as follows:
![table](Week07/P8.jpg)

### a. Provide a forwarding table that has five entries, uses longest prefix matching, and forwards packets to the correct link interfaces.

| Prefix Match | Interface |
| ------------ | :-------: |
| 11100000 00 | 0 |
| 11100000 01000000 | 1 |
| 11100000 01000001 | 2 |
| 11100001 01 | 2 |
| otherwise | 3 |

### b. Describe how your forwarding table determines the appropriate link interface for datagrams with destination addresses:

**11001000 10010001 01010001 01010101**: this does not match the prefix for any interface in the table, so it will go to interface 3
**11100001 01000000 11000011 00111100**: the first 10 bits match the 10-bit prefix for interface 2, so it will go to interface 2
**11100001 10000000 00010001 01110111**: this does not match the prefix for any interface in the table, so it will go to interface 3

## P9. Consider a datagram network using 8-bit host addresses. Suppose a router uses longest prefix matching and has the following forwarding table: (For each of the four interfaces, give the associated range of destination host addresses and the number of addresses in the range.)

|Prefix Match|Interface|
| :--------: | :-----: |
| 00 | 0 |
| 010 | 1 |
| 011 | 2 |
| 10 | 2 | 
| 11 | 3 |

| Interface | Bit Range | Number of Addresses |
| :-------: | --------- | :-----------------: |
| 0 | 00000000 --> 00111111 | 64 |
| 1 | 01000000 --> 01011111 | 32 |
| 2 | 01100000 --> 10111111 | 96 |
| 3 | 11000000 --> 11111111 | 64 |


## P11. Consider a router that interconnects three subnets: Subnet 1, Subnet 2, and Subnet 3. Suppose all of the interfaces in each of these three subnets are required to have the prefix 223.1.17/24. Also suppose that Subnet 1 is required to support at least 60 interfaces, Subnet 2 is to support at least 90 interfaces, and Subnet 3 is to support at least 12 interfaces. Provide three network addresses (of the form a.b.c.d/x) that satisfy these constraints.
Subnet 1: 223.1.17.192/26
- 60 interfaces needed, needs 6 bits

Subnet 2: 223.1.17.128/25
- 90 interfaces needed, needs 7 bits

Subnet 3: 223.1.17.208/28
- 12 interfaces needed, needs 4 bits