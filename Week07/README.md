# Week 7: The Network Layer: Data Plane

*Gabriel Spencer | Fall 2023*

This week we went over some basics of the network layer such as how subnets and routers work, and how DHCP works.

* [Review Questions](Week07/ReviewQs.md)
* [Learning Goals](Week07/LearningGoals.md)
* [Lab: Router Simulator](Week07/lab)
* [Back to Overview](../README.md)
