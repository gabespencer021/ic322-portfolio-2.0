# Review Questions

## R5. What information is used by a process running on one host to identify a process running on another host?

In order to properly identify another process, both the address and an identifier for the process running has to be specified. A process uses an IP address to identify the address of a host and a port number to identify what process is running on the host.

## R8. List the four broad classes of services that a transport protocol can provide. For each of the service classes, indicate if either UDP or TCP (or both) provides such a service.

1. Reliable data transfer: provided by TCP
2. Throughput: provided by UDP
3. Timing: provided by UDP
4. Security: provided by TCP

## R11. What does a stateless protocol mean? Is IMAP stateless? What about SMTP?

A stateless protocol is when a server maintains no information about the clients. Neither IMAP nor SMTP are stateless protocols because they maintain information about clients over time.

## R12. How can websites keep track of users? Do they always need to use cookies?

Websites use cookies to keep track of users. The host stores an id for a certain website, and when it sends a GET request, it includes the id, and then the server provides a customized response based on the information it has stored for that id. Websites do not need to use cookies, but if they do not, they cannot make a custom experience for the user.

## R13. Describe how Web caching can reduce the delay in receiving a requested object. Will Web caching reduce the delay for all objects requested by a user or for only some of the objects? Why?

A web cache stores recently used objects so that when they are requested, the web cache does not have to send a request to the server, but just has to send the requested object back to the client. In this case, it acts like a server. If it does not have the requested object stored, then it acts as a client and sends a GET request to the actual server. Once it receives a response, it sends that back to the host which made the initial request. A web cache reduces the delay for clients which request things that it has stored, but not for the clients which require it to make a GET request itself. It also can decrease delay because it lowers the amount of traffic that the server itself has to handle, so when requests are sent to the server, they do not have to wait as long.

## R14. Telnet into a Web server and send a multiline request message. Include in the request message the If-modified-since: header line to force a response message with the 304 Not Modified status code.

I submitted the following request: 

>GET /kurose_ross/header_graphic_book_8E_3.jpg HTTP/1.1<br>
Host: gaia.cs.umass.edu<br>
If-modified-since: Fri, 30 Jul 2021 16:33:35 GMT

The response from the server was this: 

>HTTP/1.1 304 Not Modified<br>
Date: Thu, 07 Sep 2023 02:57:26 GMT<br>
Server: Apache/2.4.6 (CentOS) OpenSSL/1.0.2k-fips PHP/7.4.33 mod_perl/2.0.11 Perl/v5.16.3<br>
ETag: "2e075-5c859c7e6f5c0"

## R26. In Section 2.7, the UDP server described needed only one socket, whereas the TCP server needed two sockets. Why? If the TCP server were to support *n* simultaneous connections, each from a different client host, how many sockets would the TCP server need?

The UDP server just waits to receive anything sent to the proper IP address and port number, so it only needs one socket open for that. The TCP server has one open socket that is for receiving TCP requests. Once a request is received, it opens a new socket that it dedicates to the user that made the request. If a TCP server wanted to support *n* connections at the same time, it would need *n+1* sockets because it needs one dedicated to each client and one that is open and waiting to receive requests.<br>