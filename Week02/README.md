# Week 2: The Application Layer

*Gabriel Spencer | Fall 2023*

This week we studied the Application Layer. This included looking at TCP and UDP, HTTP requests and responses, cookies, and web caches.

* [Review Questions](Week02/ReviewQs.md)
* [Learning Goals](Week02/LearningGoals.md)
* [Lab: Build a Server](Week02/lab/ServerLab.md)
* Back to [Overview](../README.md)
