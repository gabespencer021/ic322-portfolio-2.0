# Learning Goals

## I can explain the HTTP message format, including the common fields.

There are two types of HTTP messages: requests and responses.
<br><br>
Here is an example of an HTTP request:
<br><br>
>GET /somedir/page.html HTTP/1.1<br>
Host: www.someschool.edu<br>
Connection: close<br>
User-agent: Mozilla/5.0<br>

<br>
The first line is called the request line (the one that begins with GET). The other lines are called header lines. Every request line has three fields: the method field, the URL field, and the HTTP version field. The method field: can have different values such as GET, POST, HEAD, PUT, and DELETE. Most of the time the method field is GET (used to request an object which is identified by the URL field). The URL field contains the destination to which the request is being sent. The version field contains which HTTP version. the browser is using.<br><br>

The "Host:" header line specifies where the requested object resides. The "Connection:" line specifies whether or not the connection should be persistent (not persistent in this case). The "User-agent:" line specifies what type of browser is making the request.<br><br>

Here is an example of an HTTP response:<br><br>

>HTTP/1.1 200 OK<br>
Connection: close<br>
Date: Tue, 18 Aug 2015 15:44:04 GMT<br>
Server: Apache/2.2.3 (CentOS)<br>
Last-Modified: Tue, 18 Aug 2015 15:11:03<br>
Content-Length: 6821<br>
Content-Type: text/html<br><br>
(data data data data ...)

<br><br>
This has a status line, six header lines, and the entity body. The status line has three parts: the version field, the status code, and the status message. The version field is the same as the version field for the HTTP request. The status code and message say whether or not there was an error, and if there was, what the error was.<br><br>

The "Connection: close" line tells the client that the server is going to close the TCP connection after it sends the message. The "Date:" line says when the message was created and sent by the server. The "Server:" line indicated what type of server generated the message. The "Last-Modified:" line says when the object was created or last modified. The "Content-Length:" line indicates the size of the object being sent (in bytes). the "Content-Type:" line here indicates that the object in the entity body is HTML.<br><br>

The part that says "(data data data data ...)" is the entity body and it contains the object that was requested.

## I can explain the difference between HTTP GET and POST requests and why you would use each.

HTTP GET requests have the information of the request in the header of the GET request and the entity body is empty. HTTP POST requests store the body of the request in the entity body. POST requests are typically used for forms, and all the information from the form is in the entity body. GET can be used for forms, and then the values from the form are in the URL, so you can sometimes see some very long URLs. When there is any sensitive information (such as a username and password) in a form, POST should definitely be used because then the values are not visible in the URL line.

## I can explain what CONDITIONAL GET requests are, what they look like, and what problem they solve.

Conditional GET requests are when a web cache sends a GET request with a line after the Host line that says "If-modified-since:" with a date and time, for example: 

```
GET /fruit/kiwi.gif HTTP/1.1
Host: www.exotiquecuisine.com 
If-modified-since: Wed, 9 Sep 2015 09:23:24
```

This is to make sure that when a web cache sends an object to a host, the object has not been changed or updated by the server. If the object has not been updated, the server's response will have "304 Not Modified" in the status line and will have an empty entity body. If it has been updated, the server will send the updated object to the web cache.

## I can explain what an HTTP response code is, how they are used, and I can list common codes and their meanings.

An HTTP response code is also known as a status code, and its purpose is to give the client that issued the GET request an update on whether or not there was an issue and what the issue was. Some common status codes are 
- 200 OK: the request succeeded and the information is in the response.
- 301 Moved Permanently: the requested object has been moved. The "Location:" line should provide an updated URL that the client's software should automatically retrieve.
- 400 Bad Request: general error meaning that the server couldn't understand the request
- 404 Not Found: the requested document doesn't exist on this server
- 505 HTTP Version Not Supported: the server does not support the specified HTTP version

## I can explain what cookies are used for and how they are implemented.

Cookies are used to keep track of a user's data and history on certain websites to make a person's experience on that website more personalized. The client will save an id for the cookies of a specific URL on the host and will include that id within a GET request for that website. The server associates that id with data and history, and gives a more specialized response for the specific id that is part of the GET request. This is how websites "remember" what you searched for and what you did last time you were there.

## I can explain the significance HTTP pipelining.

HTTP pipelining allows for multiple requests to be sent to the same TCP connection without waiting for a response. This is important because it can greatly reduce the delay of waiting for a response for each request before the next request is able to go through.

## I can explain the significance of CDNs, and how they work.

A CDN is a Content Distribution Network. It is used to store things such as large amounts of video data. They are strategically located and they make it easier and shorter for clients to access the data that they have. If the data were stored in a central location and not in many CDNs, then each time it would be streamed, it would be sent out, frequently to similar places across the same parts of the network (repetition is bad) and it would be more likely to hit a bottleneck that would cause the video to not run cleanly. CDNs store the video data so that it does not have to be sent nearly as far and has a much smaller chance of hitting a bottleneck. If a CDN does not have something that was requested, it will request it, and then it will keep a copy, and if it is out of space, it will remove other things that are not frequently requested. This way it tries to keep a good set of data that can provide the fastest service to the most people.
>"If a client requests a video from a cluster that is not storing the video, then the cluster retrieves the video (from a central repository or from another cluster) and stores a copy locally while streaming the video to the client at the same time. Similar Web caching, when a cluster’s storage becomes full, it removes videos that are not frequently requested." - from the textbook