# Wireshark DNS Lab

Back to [Week 2](Week02/Week2.md)

## Introduction

This lab was making a very basic TCP server that could respond to a couple different HTTP requests. It can display an html website, run a script on that website, and can open images (.jpg and .png) to view. I learned more about how a socket works and how a TCP connection works with regards to being connection-oriented, eg. I understand better how a server opens a socket for every client so that each one has its own connection.

## Collaboration

- I looked at [Soleil's server program](https://gitlab.com/soleilxie1/ic322-portfolio/-/blob/master/week2/lab-build-a-server/TCPServer.py?ref_type=heads) for help to see how to properly modify the program to be able to handle images.

- I also used ChatGPT to help understand why my server wasn't working (chat conversation linked [here](https://chat.openai.com/c/84f0295f-f408-48f9-8d26-69a89ccba488)).

## Process

I followed the instructions for building the server on the course website in Week2 - [Lab: Build a Server](https://ic322.com/#/assignments/week02/lab-build-a-server).

## Questions

### 1: Why are we focusing on the TCP server in this lab rather than the UDP server?

TCP is much more commonly used, and has a lot more advantages than UDP. The only real advantage of UDP is speed, and for my simple server, I do not need to be concerned with it being fast. For this server, the connection-oriented nature of TCP as well as the guarantee of packets not being lost is important so that it will receive the files properly and will not have any errors. The benefits of TCP here outweigh the benefits of UDP. The other reason is that this lab is using HTTP, and HTTP uses TCP not UDP.

### 2: Look carefully at Figure 2.9 (General format of an HTTP response message). Notice that there's a blank line between the header section and the body. And notice that the blank line is two characters: cr and lf. What are these characters and how do we represent them in our Python response string?

`cr` is 'carriage return', and `lf` is 'line feed'. In the Python response that my server sends, the `cr` is `\r` and the `lf` is `\n`. Seeing `\r\n` does not look very normal because that is what is typically invoked when you hit the 'Enter' key.

### 3: When a client requests the `/index.html` file, where on your computer will your server look for that file?

The computer looks for that file in the same place that the server program is being run from which means whatever path is specified to the file will originate in the same directory where the server is being run. In this case, it will look in `~/ic322/Portfolio/Week02/lab/` for the "index.html" file.