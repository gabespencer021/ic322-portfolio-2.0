# Running my server

The command to run the server is just `python3 myServer.py`. Any images that are requested should be included in the directory so the server program can access them. The server should run continuously until you interrupt it with Ctrl+c or Ctrl+z.