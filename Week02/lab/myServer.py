from socket import *
import sys

serverPort = 12005
serverSocket = socket(AF_INET,SOCK_STREAM)
serverSocket.bind(('',serverPort))
serverSocket.listen(1)
print('The server is ready to receive')

while True:
    connectionSocket, addr = serverSocket.accept()
    sentence = connectionSocket.recv(1024).decode()
    print(sentence)

    filename = sentence.split()[1]
    
    response = "HTTP/1.1 200 OK\r\n"

    try:
        
        extension = filename.split(".")[1] # split the filename by "."
        content = ""

        if extension in ("jpg", "jpeg"):
            response += "Content-type: image/jpeg\r\n\r\n" # header with content type
            f = open(filename[1:],"rb")
            content = f.read()
        elif extension == "png":
            response += "Content-type: image/png\r\n\r\n" # header with content type
            f = open(filename[1:],"rb")
            content = f.read()
        else:
            response += "Content-type: text/html\r\n\r\n" # header with content type
            f = open(filename[1:])
            content = f.read().encode()

        connectionSocket.send(response.encode())
        connectionSocket.send(content)

    except:
        #send file-not-found error
        response = "HTTP/1.1 404 Not Found\r\n\r\n"
        connectionSocket.send(response.encode())
        
    connectionSocket.close()

serverSocket.close()

sys.exit() # terminate program after sending data