# Week 5: Transport Layer Part II

*Gabriel Spencer | Fall 2023*

This week we covered TCP's methods for congestion control and flow control: what they are and how they work.

* [Review Questions](Week05/ReviewQs.md)
* [Learning Goals](Week05/LearningGoals.md)
* [Partner Feedback](https://gitlab.com/PJAsjes/ic322-portfolio/-/issues/9)
* [Back to Overview](../README.md)
