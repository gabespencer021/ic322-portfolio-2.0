# Review Questions

## R17. Supp​ose two TCP connections are present over some bottleneck link of rate R bps. Both connections have a huge file to send (in the same direction over the bottleneck link). The transmissions of the files start at the same time. What transmission rate would TCP like to give to each of the connections?
TCP would ideally like to give equal transmission rates to each of the connections (R/2), and that should basically occur, but, due to how congestion control works in TCP, the amount of bandwidth will fluctuate between the two so that it may not be even at a given time.

## R18. True or fa​lse? Consider congestion control in TCP. When the timer expires at the sender, the value of cwnd is set to one half of its previous value.
False. In the case of an expired timer, cwind should revert all the way back down to 1 MSS. If the sender were to receive three, duplicate ACKs, then the cwind value would be set to half of its previous value.

## P27. Host A and B are communicating over a TCP connection, and Host B has already received from A all bytes up through byte 126. Suppose Host A then sends two segments to Host B back-to-back. The first and second segments contain 80 and 40 bytes of data, respectively. In the first segment, the sequence number is 127, the source port number is 302, and the destination port number is 80. Host B sends an acknowledgment whenever it receives a segment from Host A.

**a.** In the second segment sent from Host A to B, what are the sequence number, source port number, and destination port number?
- Sequence number: 207 - the byte number of the first byte of the message, acknowledging proper receipt of all bytes up to 206
- Source port number: 302 - the port number of host A
- Destination port number: 80 - the port number of host B

**b.** If the first segment arrives before the second segment, in the acknowledgment of the first arriving segment, what is the acknowledgment number, the source port number, and the destination port number?
- ACK number: 207 - the next byte expected, acknowledging proper receipt of all bytes up to 206
- Source port number: 302 - the port number of host A
- Destination port number: 80 - the port number of host B

**c.** If the second segment arrives before the first segment, in the acknowledgment of the first arriving segment, what is the acknowledgment number?
- The ACK number would be 127 because that is the oldest expected byte number that has not arrived yet.

**d.** Suppose the two segments sent by A arrive in order at B. The first acknowledgment is lost and the second acknowledgment arrives after the first timeout interval. Draw a timing diagram, showing these segments and all other segments and acknowledgments sent. (Assume there is no additional packet loss.) For each segment in your figure, provide the sequence number and the number of bytes of data; for each acknowledgment that you add, provide the acknowledgment number.
- <img src="timingDiagram.jpg" alt="Timing Diagram" width="302" height="403">

## P33. In Section 3.5.3, we discussed TCP’s estimation of RTT. Why do you think TCP avoids measuring the SampleRTT for retransmitted segments?
If segments are being retransmitted, it frequently means that there is congestion and when there is congestion, a SampleRTT will be very high and will not provide useful data. The segment that is getting retransmitted also has already had a SampleRTT measured, so it's data contribution has already been given, and if it were to be measured again, that segment would be contributing more heavily than the others to the weighted average.

## P36. In Section 3.5.4, we saw that TCP waits until it has received three duplicate ACKs before performing a fast retransmit. Why do you think the TCP designers chose not to perform a fast retransmit after the first duplicate ACK for a segment is received?
If packets arrive out of order, the ACK sent could send duplicate ACKs for the segment that was sent first but arrived later, and then it would be retransmitted unnecessarily because it arrived correctly the first time, just out of order.

## P40. Consider Figure 3.61. Assuming TCP Reno is the protocol experiencing the behavior shown above, answer the following questions. In all cases, you should provide a short discussion justifying your answer.
<img src="figure361.jpg" alt="Figure 3.61" width="380" height="226">

**a.** Identify the intervals of time when TCP slow start is operating.<br>
- Slow start goes from transmission round 1 to 6 and from 23 to 26.

**b.** Identify the intervals of time when TCP congestion avoidance is operating.<br>
- Congestion avoidance is from transmission round 6 to 22.

**c.** After the 16th transmission round, is segment loss detected by a triple duplicate ACK or by a timeout?<br>
- After the 16th transmission round, segment loss is detected by a triple duplicate ACK because when there is a triple duplicate ACK, the cwind does not drop all the way back to 1 like it would if there were a timeout. This is because when we are still receivig ACKs, we know that the receiver is still receiving packets

**d.** After the 22nd transmission round, is segment loss detected by a triple duplicate ACK or by a timeout?<br>
- After the 22nd transmission round, segment loss is detected by a timeout because when there is a timeout, the cwind goes back down to slow start at 1. This is done because if there is a timeout, the receiver is not receiving any more packets because if it were, we would be receiving duplicate ACKs.

**e.** What is the initial value of *ssthresh* at the first transmission round?<br>
- The initial value of *ssthresh* at the first transmisson round is 32 because that is where the slow start phase changes to the congestion control phase.

**f.** What is the value of *ssthresh* at the 18th transmission round?<br>
- The value of *ssthresh* at the 18th transmission round is 21 because that is half of where cwind was before it encountered the duplicate ACKs that caused it to drop.

**g.** What is the value of *ssthresh* at the 24th transmission round?<br>
- The value of *ssthresh* at the 24th transmission round is 14 because that is half of what it was when it encountered a timeout.

**h.** During what transmission round is the 70th segment sent?<br>
- The 70th segment is sent during the 7th transmission round because 1+2+4+8+16+32=63 segments during the first 6 transmission rounds, so since the 7th round sends more than 7 segments, the 70th segment is during the 7th round.

**i.** Assuming a packet loss is detected after the 26th round by the receipt of a triple duplicate ACK, what will be the values of the congestion window size and of *ssthresh*?<br>
- The congestion window will be 7 because it will be half of the size at the dropped packet (8/2=4) plus the number of packets that were received after the dropped one (3 because there were three duplicate ACKs). The new value of *ssthresh* is 7 because it was 14 before and when a packet is dropped, *ssthresh* is divided by two.

**j.** Suppose TCP Tahoe is used (instead of TCP Reno), and assume that triple duplicate ACKs are received at the 16th round. What are the ssthresh and the congestion window size at the 19th round?<br>
- *ssthresh* would be 21 because that is half of what cwind was when it dropped a packet. The congestion window size would be 4 because cwind would go to zero and restart the slow start phase at the 17th round, so cwind would be 4 at the 19th round.

**k.** Again, suppose TCP Tahoe is used, and there is a timeout event at the 22nd round. How many packets have been sent out from the 17th round till the 22nd round, inclusive?<br>
- 17:1, 18:2, 19:4, 20:8, 21:16, 22:32<br>
- 1+2+4+8+16+32=63 packets.
