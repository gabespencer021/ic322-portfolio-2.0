# Learning Objectives

## I can explain how a the DNS system uses local, authoritative, TLD, and root servers to translate an IP address into a hostname.

The process for a client to get the IP address of a certain domain is that the client contacts a root server which gives it an IP address for a TLD server. The client then contacts the TLD server which returns an IP address of an authoritative server. The client then contacts the authoritative server which gives the client the IP address of the hostname for the domain they are trying to reach.

- **Local:** Local DNS servers do not technically belong to the hierarchy of servers, but this does not mean they are not important. Every ISP has a local DNS server. When a host connects to an ISP, the ISP gives the host the IP address of one of its local DNS servers.
- **Authoritative:** authoritative servers return the IP address for the hostname of the domain that the client is trying to reach. Every organization with a public host (like a Web server or mail server) must provide public DNS records that associate the hosts with their IP addresses.
- **TLD:** TLD (Top Level Domain) servers provide the IP addresses of authoritative servers after the client contacts them. There are TLD servers for all the top-level domains such as com, org, net, edu, and gov, as well as for all of the country top-level domains such as uk, fr, ca, and jp.
- **Root:** root DNS servers provide the IP addresses of TLD servers when the client is requesting an IP address.

The hierarchy of how these are arranged can be seen below.

![DNS Hierarchy](Week03/DNS_hierarchy.png)

## I can explain the role of each DNS record type.

Resource records are how DNS servers store their data. A resource record is a four-tuple that has these fields:

`(Name, Value, Type, TTL)`

TTL is the time to live of the resource record or how long it will be before it is removed from a cache.
The four types of DNS records (A, NS, CNAME, and MX) determine the meaning of *Name* and *Value*.

- **Type A:** *Name* is a hostname and *Value* is the IP address for that hostname.
- **Type NS:** *Name* is a domain and *Value* is the hostname of an authoritative DNS server that knows how to obtain the IP address for the hosts in the domain.
- **Type CNAME:** *Value* is a canonical hostname for the alias hostname *Name*.
- **Type MX:** *Value* is the canonical name of a mail server that has an alias hostname *Name*.
    - CNAME and MX basically do the same thing, CNAME for hosts and MX for mail servers

## I can explain the role of the SMTP, IMAP, and POP protocols in the email system.

- **SMTP** (Standard Mail Transfer Protocol) is the principal application-layer protocol forInternet electronic mail. It uses TCP to transfer mail from the sender's mail server to the recipient's mail server. SMTP can act as both the client and the server the client when it is sending mail and the server when it is receiving.
- **IMAP** (Internet Mail Access Protocol) is used by a client to retrieve incoming email from their mail server.
- **POP** (Post Office Protocol) is a protocol that does basically the same thing as IMAP. The main difference is that when the mail is retrieved from the server, POP deletes the message from the server while IMAP keeps the message on the server.

## I know what each of the following tools are used for: nslookup, dig, whois.

**nslookup** is a tool that allows you to send DNS messages. You query a domain name and it returns a bunch of information about that domain including the IP address (or addresses). It gives both IPv4 and IPv6 addresses. Here is an example:

```
m255964@ward-rweb-09:~/ic322/Portfolio/Week03$ nslookup netflix.com
Server:         127.0.0.53
Address:        127.0.0.53#53

Non-authoritative answer:
Name:   netflix.com
Address: 3.211.157.115
Name:   netflix.com
Address: 3.225.92.8
Name:   netflix.com
Address: 54.160.93.182
Name:   netflix.com
Address: 2600:1f18:631e:2f82:c8cd:27b2:ac:8dbf
Name:   netflix.com
Address: 2600:1f18:631e:2f80:77e5:13a7:6533:7584
Name:   netflix.com
Address: 2600:1f18:631e:2f84:ceae:e049:1e:6a96
```

**dig** is another tool for sending messages to DNS servers and performing DNS lookups. It has options for what you can query (as found on [this](https://www.cyberciti.biz/faq/linux-unix-dig-command-examples-usage-syntax/) website), such as the following:

```
$ dig Hostname
$ dig DomaiNameHere
$ dig @DNS-server-name Hostname
$ dig @DNS-server-name IPAddress
$ dig @DNS-server-name Hostname|IPAddress type
```
In this example, 
1. `DNS-server-name` is the name or IP address of the name server to ask dns answers
2. `Hostname|IPAddress` is the name of the resource record that is to be looked up
3. `type` is the type of dig command query, such as A, MX, NS, and CNAME
- This information also came from [this](https://www.cyberciti.biz/faq/linux-unix-dig-command-examples-usage-syntax/) website

You can also run the same command from the browser with a website like [digwebinterface.com](https://www.digwebinterface.com/)
<br>
Below is an example of a dig command run on the lab machine.

```
m255964@ward-rweb-09:~/ic322/Portfolio/Week03$ dig netflix.com

; <<>> DiG 9.18.12-0ubuntu0.22.04.2-Ubuntu <<>> netflix.com
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 22370
;; flags: qr rd ra; QUERY: 1, ANSWER: 3, AUTHORITY: 0, ADDITIONAL: 1

;; OPT PSEUDOSECTION:
; EDNS: version: 0, flags:; udp: 65494
;; QUESTION SECTION:
;netflix.com.                   IN      A

;; ANSWER SECTION:
netflix.com.            5       IN      A       3.225.92.8
netflix.com.            5       IN      A       3.211.157.115
netflix.com.            5       IN      A       54.160.93.182

;; Query time: 0 msec
;; SERVER: 127.0.0.53#53(127.0.0.53) (UDP)
;; WHEN: Wed Sep 13 21:26:40 EDT 2023
;; MSG SIZE  rcvd: 88
```

**whois** is a command to find out information about a domain name such as who or what corporation owns it. It can be accessed through an online website (like [whois.com](whois.com)) or from the command line with the command `whois domain.com`.

Here is the example output from running the command from [whois.com](whois.com) on netflix.com:

| Category      |   Value                       |
| ------------- | ----------------------------- |
| Domain:       |   netflix.com                 |
| Registrar:    |   MarkMonitor Inc.            |
| Registered On:|   1997-11-11                  |
| Expires On:   |   2023-11-10                  |
| Updated On:   |   2023-05-17                  |
| Status:       |   clientDeleteProhibited<br>clientTransferProhibited<br>clientUpdateProhibited<br>serverDeleteProhibited<br>serverTransferProhibited<br>serverUpdateProhibited<br> |
| Name Servers: |   ns-1372.awsdns-43.org<br>ns-1984.awsdns-56.co.uk<br>ns-659.awsdns-18.net<br>ns-81.awsdns-10.com |