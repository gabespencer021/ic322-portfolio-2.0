# Wireshark DNS Lab

## Introduction

This lab was an introduction to exploring DNS with wireshark. I looked at DNS queries and responses, as well as a couple different types of DNS queries, such as type A and type NS.

## Collaboration

I did not use outside information to complete this lab. I did check answers that I was unsure about with answers from [Soleil's Lab](https://gitlab.com/soleilxie1/ic322-portfolio/-/tree/master/week3/lab-wireshark-dns).

## Process

For this lab, I followed the instructions in the DNS wireshark lab from [this website](https://gaia.cs.umass.edu/kurose_ross/wireshark.php).

## Questions

### 1. Run *nslookup* to obtain the IP address f the web server for the Indian Institute of Technology in Bombay, India: www.iitb.ac.in. What is the IP address of www.iitb.ac.in?<br>

103.21.124.10

### 2. What is the IP address of the DNS server that probided the answer to your *nslookup* command in question 1 above?<br>

75.75.75.75

### 3. Did the answer to your *nslookup* command in question 1 above come from an authoritative or non-authoritative server?<br>

It came from a non-authoritative server.

### 4. a. Use the *nslookup* command to determine the name of the authoritative name server for the iitb.ac.in domain. b. What is that name? (If there are more than one authoritative servers, what is the name of the first authoritative server returned by *nslookup*)? If you had to find the IP address of that authoritative name server, how would you do so?
    
a. The command to determine the name of the authoritative server for the iitb.ac.in domain is `nslookup -type-NS iitb.ac.in`.

b. dns1.iitb.ac.in

c. Use the command `nslookup dns1.iitb.ac.in`.

### 5. Locate the first DNS query message resolving the name gaia.cs.umass.edu. What is the packet number in the trace for the DNS query message? Is this query message sent over UDP or TCP?

Packet number: 15
The query message was sent over UDP.

### 6. Now locate the corresponding DNS response to the initial DNS query. What is the packet number in the trace for the DNS response message?

Packet number: 17
The response message was received via UDP.

### 7. What is the destination port for the DNS query message? What is the source port of the DNS response message?

Both are port 53.

### 8. To what IP address is the DNS query message sent?

75.75.75.75

### 9. Examine the DNS query message. How many “questions” does this DNS message contain? How many “answers” answers does it contain?

It contains one question and zero answers.

### 10. Examine the DNS response message to the initial query message. How many “questions” does this DNS message contain? How many “answers” answers does it contain?

It contains one question and one answer.

### 11. The web page for the base file http://gaia.cs.umass.edu/kurose_ross/ references the image object http://gaia.cs.umass.edu/kurose_ross/header_graphic_book_8E_2.jpg , which, like the base webpage, is on gaia.cs.umass.edu. What is the packet number in the trace for the initial HTTP GET request for the base file http://gaia.cs.umass.edu/kurose_ross/? What is the packet number in the trace of the DNS query made to resolve gaia.cs.umass.edu so that this initial HTTP request can be sent to the gaia.cs.umass.edu IP address? What is the packet number in the trace of the received DNS response? What is the packet number in the trace for the HTTP GET request for the image object http://gaia.cs.umass.edu/kurose_ross/header_graphic_book_8E2.jpg? What is the packet number in the DNS query made to resolve gaia.cs.umass.edu so that this second HTTP request can be sent to the gaia.cs.umass.edu IP address? Discuss how DNS caching affects the answer to this last question.

The initial GET request is packet number 22. The packet number of the initial DNS query made to resolve gaia.cs.umass.edu is 15. The received DNS response was in packet number 17. The packet number for the request for the image object http://gaia.sc.umass.edu/kurose_ross/header_graphic_book_8E2.jpg is 205. There is no DNS query made to resolve gaia.cs.umass.edu so that a second HTTP request can be sent to the gaia.cs.umass.edu IP address because the IP address is still stored in the cache from the initial query. DNS caching is the reason that there is not a second query to resolve gaia.cs.umass.edu.

### 12. What is the destination port for the DNS query message? What is the source port of the DNS response message?

Both are 53

### 13.  To what IP address is the DNS query message sent? Is this the IP address of your default local DNS server?

It is sent to 75.75.75.75. This is not the IP address of my local DNS server.

### 14. Examine the DNS query message. What “Type” of DNS query is it? Does the query message contain any “answers”?

It is of type A which means that the query is requesting an IP address for the specified hostname. The query message does not contain any answers.

### 15. Examine the DNS response message to the query message. How many “questions” does this DNS response message contain? How many “answers”?

It contains one question and one answer.

### 16. To what IP address is the DNS query message sent? Is this the IP address of your default local DNS server?

The DNS query is sent to 75.75.75.75. This is not my default local DNS server, but since it is the same as the ones from the above questions and I am using the file provided for the lab, this is probably the local DNS server for whatever machine was used to capture these packets.

### 17. Examine the DNS query message. How many questions does the query have? Does the query message contain any “answers”?

The DNS query contains one question and zero answers. 

### 18. Examine the DNS response message. How many answers does the response have? What information is contained in the answers? How many additional resource records are returned? What additional information is included in these additional resource records?

The DNS response has three answers. These answers contain the following information:

```
umass.edu: type NS, class IN, ns ns1.umass.edu
umass.edu: type NS, class IN, ns ns3.umass.edu
umass.edu: type NS, class IN, ns ns2.umass.edu
```

There are three additional resource records returned. They contain the following information: 

```
ns2.umass.edu: type A, class IN, addr 128.119.10.28
ns1.umass.edu: type A, class IN, addr 128.119.10.27
ns3.umass.edu: type A, class IN, addr 128.103.38.68
```