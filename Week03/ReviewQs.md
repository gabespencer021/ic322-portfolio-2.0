# Review Questions

## R16: Suppose Alice, with a Web-based e-mail account (such as Hotmail or Gmail), sends a message to Bob, who accesses his mail from his mail server using IMAP. Discuss how the message gets from Alice’s host to Bob’s host. Be sure to list the series of application-layer protocols that are used to move the message between the two hosts.

1. Alice accesses her user agent, writes the email message she wants to send, provides the email address she is sending to (Bob's), and tells the user agent to send it.
2. The message is then sent by the user agent to Alice's mail server where it waits in a message queue to be sent.
3. Alice's mail server sees that there is a message in the queue and (as the client) opens an TCP connection with an SMTP server running on Bob's mail server.
4. After the connection is properly established, the SMTP client sends Alice's message through the TCP connection to Bob's server.
5. The server side of the SMTP connection receives Alice's message, and Bob's mail server places it into his inbox.
6. Bob uses his user agent to read the message.

The unexplained issue now is how Bob accesses his messages that are on his mail server using his user agent because SMTP is only a push protocol, not a pull. In this case, he uses IMAP (Internet Mail Access Protocol). This allows him to manage his email as he would like.

## R18: What is the HOL blocking issue in HTTP/1.1? How does HTTP/2 attempt to solve it?

HOL (Head of Line) blocking is when there is a persistent HTTP/1.1 connection that needs to send multiple objects for a Web page, and a large object (like a video or large image) takes a long time to go through (made even worse if there is some form of bottleneck) causing the Web page to have to wait to load. While the Web page is waiting for the large object to load, none of the objects that are after that one can be loaded even if they are small and would load quickly. HTTP/2 solves this issue by breaking every message into small frames, and to alternate between responses for different requests on the same TCP connection. Say, for example, there is a video clip of 1000 frames followed by eight other objects each of 2 frames. In HTTP/1.1, The small objects would have to wait until all 1000 frames of the video were sent before they could send, but in HTTP/2, it would send 1 frame of the video, then one frame from each of the other objects, then one more of the video, followed by one more from each object, and so on until there are no more frames to send. In this case of HTTP/2, it would take only 18 frames being sent for all the objects except the video to load so that the video is not stopping everything else, whereas in the HTTP/1.1 example, it would take 1016 frames before those were all loaded. The user-perceived delay for HTTP/2 is lower than that of HTTP/1.1. 

## R24: CDNs typically adopt one of two different server placement philosophies. Name and briefly describe them.

* **Enter Deep:** this philosophy puts server clusters in ISPs all over he world. The idea here is that the CDNs will be close to end users, and thus the user-perceived delay and the throughput will be improved by decreasing the number of links and routers between the user and CDN server. This requires a lot of maintenance and management to run because it is very highly distributed and not very centralized.

* **Bring Home:** this philosophy is much less distributed than "Enter Deep". The idea of "Bring Home" is to put fewer CDNs in a more centralized location, typically at IXPs instead of at ISPs. This drastically lowers the number of CDNs needed, and thus lowers the maintenance and management overhead, but risks a greater user-perceived delay and lower user throughput.

## P16: How does SMTP mark the end of a message body? How about HTTP? Can HTTP use the same method as SMTP to mark the end of a message body? Explain.

SMTP marks the end of a message body with a period on a line by itself (see example below where "C:" is client and "S:" is server):

```
C:  Do you like ketchup?
C:  How about pickles?
C:  .
S:  250 Message accepted for delivery
```
- note that this is only the message part of the communication between the client and server.

HTTP marks the end of a message by pressing carriage return twice in a row after the last line of the message. HTTP cannot use the same method as SMTP to mark the end of a message because the end of an SMTP message never has two returns in a row, so it will still not send the message even if there is a line with just a period on it.

## P18: 

### A. What is a whois database? 

Whois is "a public database that houses the information collected when someone registers a domain name or updates their DNS settings." (quote from [this](https://www.domain.com/blog/what-is-whois-and-how-is-it-used/#:~:text=WHOIS%20is%20a%20public%20database,days%20of%20the%20early%20Internet.) website) You can access this from the command prompt with the command `whois [domain name]`.

### B. Use various whois databases on the Internet to obtain the names of two DNS servers. Indicate which whois databases you used.

1. Using [whois](whois.com), I found the DNS for Disney+ which is **ns1.twdcns.com**

2. Using [GoDaddy](godaddy.com), I found the DNS for Netflix which is **NS-1372.AWSDNS-43.ORG**

## P20: Suppose you can access the caches in the local DNS servers of your department. Can you propose a way to roughly determine the web servers (outside of your department) that are the most popular among the users in your department?

Depending on how long we have access to the caches, we can be more or less sure of what web servers are most popular. If we have access to the caches for a few days, we can monitor over those days which web servers stay in the cache the longest or are in there most often. This method could provide some very good certainty of what servers were most popular. If we have access to a couple of instances of the data in the caches, we can compare to see which servers are common among those instances. Whichever web servers appear most there are likely more popular than others. If we only have access to the cache once briefly, we can look at what servers were accessed most recently. This is not guaranteed to be very accurate, but it is more likely that the ones accessed more recently are more popular than the others because more popular ones are accessed more often, so they would tend to be more recent in general.

## P21: Suppose that your department has a local DNS server for all computers in the department. You are an ordinary user (i.e., not a network/system administrator). Can you determine if an external Web site was likely accessed from a computer in your department a couple of seconds ago? Explain.

You can determine whether or not it was recently accessed using the *dig* command. In the command line, you enter `dig [domain name]` and it will say how long the query took. You can then use *dig* on another domain name that you know has not been recently accessed. If the first one was shorter, then you can be pretty sure that the Web site has been recently accessed because the quicker response means that it was probably already in the local DNS cache.