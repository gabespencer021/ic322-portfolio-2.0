# Week 3: The Application Layer: DNS and Other Protocols

*Gabriel Spencer | Fall 2023*

This week we studied more Application Layer protocols such as DNS, SMTP, IMAP.

* [Review Questions](Week03/ReviewQs.md)
* [Learning Goals](Week03/LearningGoals.md)
* [Wireshark Lab](Week03/WiresharkLab.md)
* Back to [Overview](../README.md)
