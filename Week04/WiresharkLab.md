# Wireshark TCP Lab

## Introduction

This lab was an introduction to exploring TCP with wireshark.

## Collaboration

I collaborated a little bit with Danny Angulo to understand a question that I had.

## Process

For this lab, I followed the instructions in the TCP wireshark lab from [this website](https://gaia.cs.umass.edu/kurose_ross/wireshark.php). I used the tracer file that was provided by the lab, so I did not actually use wireshark and upload a file to gaia.cs.umass.edu.

## Questions

### 1. What is the IP address and TCP port number used b the client computer (source) that is transferrin gthe *alice.txt* file to *gaia.cs.umass.edu*? To answer this question, it's probably easiest to select an HTTP message and explore the details of the TCP packet used to carry this HTTP message, using the "details of the selected packer header window".
- Source IP address: 912.168.86.68
- Source Port number: 55639

### 2. What is the IP address of gaia.cs.umass.edu? On what port number is it sending and receiving TCP segments for this connection?
- *gaia.cs.umass.edu* IP address: 128.119.245.12
- *gaia.cs.umass.edu* Port number: 80

### 3. What is the sequence number of the TCP SYN segment that is used to initiate the TCP connection between the client computer and gaia.cs.umass.edu? (Note: this is the “raw” sequence number carried in the TCP segment itself; it is NOT the packet # in the “No.” column in the Wireshark window.  Remember there is no such thing as a “packet number” in TCP or UDP; as you know, there are sequence numbers in TCP and that’s what we’re after here.  Also note that this is not the relative sequence number with respect to the starting sequence number of this TCP session.). What is it in this TCP segment that identifies the segment as a SYN segment? Will the TCP receiver in this session be able to use Selective Acknowledgments (allowing TCP to function a bit more like a “selective repeat” receiver, see section 3.4.4 in the text)?
The first segment has relative sequence number 0 because it is the first segment sent in this connection, but its raw sequence number is 4236649187. We know this is a SYN segment because there is a line within the Transmission Control Protocol section that says `Flags: 0x002 (SYN)`. The TCP receiver could use Selective Acknowledgments because every segment has a sequence number.

### 4. What is the sequence number of the SYNACK segment sent by gaia.cs.umass.edu to the client computer in reply to the SYN? What is it in the segment that identifies the segment as a SYNACK segment? What is the value of the Acknowledgement field in the SYNACK segment? How did gaia.cs.umass.edu determine that value?
The SYNACK segment has relative sequence number 1 and raw sequence number 1068969752. This is identified as a SYNACK by the line that says `Flags: 0x012 (SYN, ACK)`. The value of the acknowledgement field is 1 (relative) or 4236649188 (raw). This is because both of those values are plus one from the sequence number of the segment that they were acknowledging.

### 5. What is the sequence number of the TCP segment containing the header of the HTTP POST command?  Note that in order to find the POST message header, you’ll need to dig into the packet content field at the bottom of the Wireshark window, looking for a segment with the ASCII text “POST” within its DATA field,.  How many bytes of data are contained in the payload (data) field of this TCP segment? Did all of the data in the transferred file alice.txt fit into this single segment?
The relative sequence number is 1 and the raw sequence number is 4236649188. The payload is 1448 bytes which is not the entire *alice.txt* file.

### 6. Consider the TCP segment containing the HTTP “POST” as the first segment in the data transfer part of the TCP connection.

**a. At what time was the first segment (the one containing the HTTP POST) in the data-transfer part of the TCP connection sent.**
It was sent at 21:43:26.716922000.

**b. At what time was the ACK for this first data-containing segment received?**
It was received at 21:43:26.745546000.

**c. What is the RTT for this first data-containing segment?**
The RTT is 21:43:26.745546000 - 21:43:26.716922000 = 0.028624 seconds.

**d. What is the RTT value of the second data-carrying TCP segment and its ACK?**
The RTT for the second data-carrying TCP segment and its ACK is 21:43:26.745551000 - 21:43:26.716923000 = 0.028628 seconds.

**e. What is the EstimatedRTT value after the ACK for the second data-carrying segment is received?**
The EstimatedRTT value is EstimatedRTT = (1 - alpha) * EstimatedRTT + alpha * SampleRTT.
    Estimated RTT = (0.875) * 0.028624 + 0.125 * 0.028628 = 0.02864025 seconds.

### 7. What is the length (header plus payload) of each of the first four data-carrying TCP segments?
They are each 1480 bytes: 1448 bytes in the payload and 32 in the header.

### 8. What is the minimum amount of available buffer space advertised to the client by gaia.cs.umass.edu among these first four data-carrying TCP segments?  Does the lack of receiver buffer space ever throttle the sender for these first four data-carrying segments?
The minimum advertised buffer space advertised is 28960, from where it says "Window: 28960". Because the smallest window after each of the first four segments is sent is 249 bytes, the lack of receiver buffer space does not throttle the sender for these first four data-carrying segments.

### 9. Are there any retransmitted segments in the trace file? What did you check for (in the trace) in order to answer this question?
There are no retransmitted segments in the trace file. I checked for duplicate ACK numbers and/or out of order ACK numbers which would have indicated retransmitted segments.

### 10. How much data does the receiver typically acknowledge in an ACK among the first ten data-carrying segments sent from the client to gaia.cs.umass.edu?  Can you identify cases where the receiver is ACKing every other received segment (see Table 3.2 in the text) among these first ten data-carrying segments?
Each ACK among the first ten data-carrying segments acknowledged 1448 bytes. I did not see any cases where he receiver ACKed more than one segment at a time.

### 11. What is the throughput (bytes transferred per unit time) for the TCP connection?  Explain how you calculated this value.
The total number of bytes is the last ACK number minus one (153426 - 1 = 153425 bytes) and the total time is the time of the last ACK minus the time of the first segment (21:43:26.884371000 - 21:43:26.692875000 = 0.191496 seconds). So the throughput is 153425/0.191496 = 801192 bytes/second.

### 12. Use the Time-Sequence-Graph(Stevens) plotting tool to view the sequence number versus time plot of segments being sent from the client to the gaia.cs.umass.edu server.  Consider the “fleets” of packets sent around t = 0.025, t = 0.053, t = 0.082 and t = 0.1. Comment on whether this looks as if TCP is in its slow start phase, congestion avoidance phase or some other phase. Figure 6 shows a slightly different view of this data.
At t = 0.025, it seems to be in slow start phase because there are very few packets being sent. At t = 0.053, it seems to be still in slow start phase. At t = 0.082, it is still in slow start phase. At t = 0.1, it is in congestion control phase because the number of packets decreased drastically from what it was right before.
<br><br>
You can view the [graph](Week04/Graph.png) to see what it showed.

### 13. These "fleets" of segments appear to have some periodicity. What can you say about the period?
The period of these "fleets" of segments starts at about 0.025 seconds, but decreases by a little bit with each fleet that is sent. By the end of the file transfer, the period looks like it is less than 0.01 seconds.

### 14. Answer each of two questions above for the trace that you have gathered when you transferred a file from your computer to gaia.cs.umass.edu.
I used the trace file provided for the lab, so I did not transfer a file from my computer to gaia.cs.umass.edu.