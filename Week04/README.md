# Week 4: The Transport Layer: TCP and UDP

*Gabriel Spencer | Fall 2023*

This week we covered TCP and UDP in more depth, looking at how they are similar and different and exploring some of the mechanisms that they use.

* [Review Questions](Week04/ReviewQs.md)
* [Learning Goals](Week04/LearningGoals.md)
* [Wireshark TCP Lab](Week04/WiresharkLab.md)
* [Back to Overview](../README.md)
