# Review Questions

## R5 Why is it that voice and video traffic is often sent over TCP rather than UDP in today’s Internet? (*Hint*: The answer we are looking for has nothing to do with TCP’s congestion-control mechanism.)

TCP is often used for media streaming today because some organizations block UDP traffic because of security risks. Video and audio streaming is also done through a browser very often, and because browsers use HTTP and HTTP uses TCP, the streaming will be done with TCP not UDP.

## R7 Suppose a process in Host C has a UDP socket with port number 6789. Suppose both Host A and Host B each send a UDP segment to Host C with destination port number 6789. Will both of these segments be directed to the same socket at Host C? If so, how will the process at Host C know that these two segments originated from two different hosts?

They will go to the same socket when they arrive at Host B because they are being sent over UDP and all UDP traffic to a given port number goes to the same socket. It knows which segment came from which host because in each segment there is a field for the sender's port number and IP address.

## R8 Suppose that a Web server runs in Host C on port 80. Suppose this Web server uses persistent connections, and is currently receiving requests from two different Hosts, A and B. Are all of the requests being sent through the same socket at Host C? If they are being passed through different sockets, do both of the sockets have port 80? Discuss and explain.

The requests will all be sent to the same socket on Host C, the welcoming socket. Once a connection is established with each host, Host C will have one socket for each of them that is solely for that connection in addition to still having the welcoming socket for new requests. When Host A and B both have their own socket, they will have both still be on port 80. This is because each socket is defined by a four-tuple: the source IP address, the source port number, the destination IP address, and the destination port number. The connection for Host A and Host B will then both have their unique four-tuple even though they share the same port number.

## R9 In our rdt protocols, why did we need to introduce sequence numbers?

Sequence numbers are used to fix the problem of potentially corrupted ACKs or NAKs. If an ACK or NAK is corrupted when the sender receives it, it will resend the current data packet because it does not know whether it was received properly or not (ACK or NAK). The problem here is that the receiver does not know whether the ACK or NAK was corrupted, so it does not know whether it is receiving a duplicate packet or is receiving a new packet. Sequence numbers solve this because when the receiver receives a packet, it knows whether or not it is a duplicate.

## R10 In our rdt protocols, why did we need to introduce timers?

Timers were introduced in case a packet were to be dropped. If a sent packet is dropped before it reaches the receiver, the sender will never know because it will not receive an ACK or a NAK. If an ACK or NAK is dropped, the sender still knows nothing about whether or not the packet arrived at its destination. A timer will measure the time from when a packet is sent, and if it is not acknowledged in some way within the timer (typically the timer is RTT plus a little bit), then the sender will resend the packet to ensure delivery.

## R11 Suppose that the roundtrip delay between sender and receiver is constant and known to the sender. Would a timer still be necessary in protocol rdt 3.0, assuming that packets can be lost? Explain.

A timer is still necessary because the sender still has to know whether or not the packet it sent ever arrived. The timer would be used to track whether or not a packet is acknowledged within a certain amount of time and if no acknowledgment is received, the sender will have to resend the packet.

## R15 Suppose Host A sends two TCP segments back to back to Host B over a TCP connection. The first segment has sequence number 90; the second has sequence number 110.

### a. How much data is the first segment

There are 20 bytes in the first segment because the sequence number is determined by adding the number of bytes in a segment to the previous sequence number. 110 - 90 = 20 bytes

### b. Suppose that the first segment is lost but the second segment arrives at B. In the acknowledgment that Host B sends to Host A, what will be the acknowledgment number?

The acknowledgment number will be 90 because in TCP the acknowledgment number will always be the next expected sequence number. Because sequence number 90 was not received, that is what is expected next.

## P3 UDP and TCP use 1s complement for their checksums. Suppose you have the following three 8-bit bytes: 01010011, 01100110, 01110100. What is the 1s complement of the sum of these 8-bit bytes? (Note that although UDP and TCP use 16-bit words in computing the checksum, for this problem you are being asked to consider 8-bit sums.) Show all work. Why is it that UDP takes the 1s complement of the sum; that is, why not just use the sum? With the 1s complement scheme, how does the receiver detect errors? Is it possible that a 1-bit error will go undetected? How about a 2-bit error?

<pre>
    01010011
  <u>+ 01100110</u>
    10111001    -->   10111001
                    <u>+ 01110100</u>
                     100101101 --> overflow wraps around --> 00101110

flip bits 00101110 --> <b>11010001</b>
</pre>

The 1s complement of the sum of the three numbers is sent with the packet as the checksum. When you add the checksum to the sum of the three values, if there have been no flipped bits, the answer will be 11111111. That is how the receiver checks for errors in the packet. A 1-bit error will never go undetected because it will alter the result so it will not be 11111111. But if there is a 2-bit error, it could possibly go undetected because that could still result in 11111111. This could happen if the bit switches occur in the same spot, for example:

<pre>
    0010  --> third bit switched -->   0000
  <u>+ 1001</u>  --> third bit switched --> <u>+ 1011</u>
    1011                               1011
</pre>