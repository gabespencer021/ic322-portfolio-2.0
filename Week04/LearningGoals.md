# Learning Goals

## I can explain how TCP and UDP multiplex messages between processes using sockets.

Multiplexing the job of gathering data chunks at the source host from different sockets, encapsulating them to create segments, and passing them to the network layer.

Demultiplexing is the job of delivering the data in a transport-layer segment to the correct socket.

TCP uses the four-tuple of identifying data to multiplex and demultiplex. The four-tuple contains the source IP address and port number, and the destination IP address and port number. 
- For demultiplexing, the four-tuple determines exactly which socket each segment should be sent to . If two segments have the same four-tuple, then they should go to the same socket. Different hosts on the same port number will have differnt IP addresses so they will not be mixed up, and segments from the same IP address with different port numbers will also be properly split up.
- For multiplexing, the four-tuple will be created using the IP address of the current host, the socket number of the current process, the IP address of where the segment needs to go, and the socket number that the segment is being sent through. It will encapsulate all this information in a segment and pass it to the network layer.

UDP uses a two-tuple for multiplexing and demultiplexing. The two-tuple contains a destination IP address and a destination port number. This results in segments from different hosts all going to the same socket as long as they have the proper port number.
- For demultiplexing, the segments will be sent to sockets according to the socket number they have. All segments with the same port number will be sent to the same socket.
- For multiplexing, the source IP address and port number are not necessary, but only the destination. These values are put into the header of the segment and it is passed to the network layer to be sent.

## I can explain the difference between TCP and UDP, including the services they provide and scenarios each is better suited to.

**TCP** is a guaranteed delivery system where every segment that is sent is verified not only that it arrived, but that it arrived unaltered/uncorrupted. TCP is also connection-oriented, and each TCP connection has its own socket that communicates with a socket on another host. TCP also has congestion control that can limit the amount of bandwidth each connection takes so that other connections are not crowded out.
- Some uses of TCP:
    - email
    - HTTP
    - remote terminal acces
    - file transfer

**UDP** provides the most basic protocol the transport layer. It has no error-checing, congestion control, or guarantee of delivery and correctness. It sends information and then doesn't worry about them any more. Once they are sent, if they drop, nothing happens and there is not really a method in place to track whether or not they were properly delivered. This does mean, though, that UDP is faster than TCP because it does not have to worry about time sending verifications back and forth.
- Some uses of UDP:
    - remote file server
    - DNS server
    - used to be video and audio streamin

## I can explain how and why the following mechanisms are used and which are used in TCP: sequence numbers, duplicate ACKs, timers, pipelining, go-back-N, selective repeat, sliding window.

**Sequence numbers** are used for cases when segments or ACKs are dropped and have to be resent. They notify the receiver which packets they have received already, and what order the packets go in. Because of sequence numbers, the receiver can now know if it is missing a packet and if it is receiving a duplicate packet.

**Duplicate ACKs** are used for the sender to know that a certain expected packet has not been received even though other packets that are after the expected one have already arrived. If the sender receives three duplicate ACKs, it will resend the next expected packet (next expected by the receiver) because three more packets after the expected one have arrived at the receiver, so the packet was probably dropped.

**Timers** are used to track when a packet was sent so that if the sender does not receive an ACK for that packet within the specified time, it will assume the packet or ACK was dropped and it will resend the packet.

**Pipelining** is used to speed up the rate that things are sent. With pipelining, a sender does not have to wait for an acknowledgment for a previous packet before sending another one. It can send multiple without receiving any responses at all.

**Go-back-N** is a protocol that allows a sender to send up to *N* unacknowledged packets without waiting for a response. ACKs in GBN will be considered cumulative acknowledgments meaning that acknowledging one packet means that all the previous packets are also good. If a timeout occurs, the sender will resend all previously sent packets that have not been acknowledged yet. If a receiver gets any packet that is not in order and/or incorrect, it will drop the packet and send an ACK for the most recent correct and in order packet it received.

**Selective repeat** is a protocol that is also limited to *N* unacknowledged packets. It is different than GBN because it does not discard packets that arrive out of order. Instead, it holds on to them and acknowledges them. Because selective repeat does not discard packets, it has to individually acknowledge every packet it receives. The sender tracks which packets are acknowledged and which are not, and it uses a timer to determine when packets need to be resent.

**Sliding Window** is when a sender is restricted to sending up to only *N* packets without acknowledgment. This will restrict the sequence numbers to a certain range.

TCP uses sequence numbers, duplicate ACKs, one timer, pipelining, and a hybrid of the go-back-N and selective repeat protocols.

## I can explain how these mechanisms are used to solve dropped packets, out-of-order packets, corrupted/dropped acknowledgements, and duplicate packets/acknowledgements.

**Dropped packets:** explained above

**Out-of-order packets:** explained above

**Corrupted/dropped acknowledgments:** If an ACK is dropped, the timer will cause a timeout, and the sender will send the packet again. If an ACK arrives corrupted, the sender will do the same thing.

**Duplicate packets/acknowledgments:** explained above