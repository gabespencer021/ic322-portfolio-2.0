# Week 9: The Network Layer: Control Plane

*Gabriel Spencer | Fall 2023*

This week we learned about the Control Plane of the Network Layer. Specifically, we looked at some algorithms that routers use to determine the fastest route to other routers.

* [Review Questions](Week09/ReviewQs.md)
* [Learning Goals](Week09/LearningGoals.md)
* [Lab: Beta Tester](Week09/Lab.md)
* [Back to Overview](../README.md)
