# Protocol Pioneer Chapter 5

## Introduction

This lab was Protocol Pioneer Chapter 5.

## Collaboration

I collaborated with Soleil for this lab.

## Process

I followed the instructions on the [course website](https://ic322.com/#/assignments/week07/protocol-pioneer/) for this lab.

## Questions

1. How did you solve the Chapters? Please copy and paste your winning strategy(s), and also explain it in English.

The strategy was for the sender to send a message if they hadn't sent one before. The routers would send East if they were able. If they were not able to send East, they would send South. Then Sawblade would check if the key had expired, and if not, would run the `self.operate_radio()` function with the key. This was a relatively simple approach, but it worked.

```
def drone_strategy(self):
    """Drones are responsible for routing messages."""

    # The `self.connected_interfaces()` method allows you to see what interfaces are connected to wires.
    number_of_connected_interfaces = len(self.connected_interfaces())
    #print(f"Drone {self.id} has {number_of_connected_interfaces} connected interfaces: {self.connected_interfaces()}")

    while(self.message_queue):
        m = self.message_queue.pop()
        
        if 'E' in self.connected_interfaces():
            self.send_message(m.text,'E')
        else:
            self.send_message(m.text,'S')

def player_strategy(self):
    """This is you. You have access to the key-generating machine. You need to send that key to Sawblade."""
    # `self.generate_key()` generates a new key. The function returns a tuple: (key, expiration). The first value
    # is the actual key (a number). The second value is the time that the key expires.
    # Every time you call the function, a new key is generated and the expiration date is set 18 ticks into the future.
    if 'key' not in self.state:
        key_expiration = self.generate_key()
        key = key_expiration[0]
        expiration = key_expiration[1]
        self.state["key"] = key
        print(f"Player: key {key} expires at {expiration}.")
        
        self.send_message(str(self.state["key"]) + ',' + str(expiration),'E')

    # If you want, you can use your heartbeat to count ticks.
    if "ticks" not in self.state:
        self.state["ticks"] = 1
    else:
        self.state["ticks"] += 1
    #print(f"The current time is {self.state['ticks']}...")

def sawblade_strategy(self):
    """Sawblade operates the radio, but need an unexpired key."""

    # If you want, you can use your heartbeat to count ticks.
    if "ticks" not in self.state:
        self.state["ticks"] = 1
    else:
        self.state["ticks"] += 1
    #print(f"The current time is {self.state['ticks']}...")

    # Sawblade has the `self.operate_radio(key)` method available to her. Call it with the value of the key that the
    # player has. Don't use an expired key, the escape pod will go on permanent lock down!
    while(self.message_queue):
        m = self.message_queue.pop()
        print(f"--- Sawblade: Msg on interface {m.interface} ---\n{m.text}\n------------------")
        print("time: " + str(self.state["ticks"]))
        key = m.text.split(",")
        if int(key[1]) > self.state["ticks"]:
            self.operate_radio(int(key[0]))

s = Chapter5(drone_strategy, player_strategy, sawblade_strategy, animation_frames=1, wait_for_user=False)
s.run()
```

2. Include a section on Beta Testing Notes.

- List any bugs you find. When you list the bugs, try to specify the exact conditions that causes them so I can reproduce them.
    - I did not find any bugs.
- Was there anything that you and others were consistently confused about? What would you do to solve that confusion?
    - The main thing that we were confused about was that I do not believe it was well-explained that a previous key expires each time a new one is generated.
- Was there too much narrative? Not enough narrative?
    - Good amount of narrative
- Was it fun?
    - This week was pretty fun, although it felt a little bit easy because we cheated a little bit.
- Really, just let me know how I can improve the game.
    - No particular recommendations besides making it more clear that a key expires when another key is generated.