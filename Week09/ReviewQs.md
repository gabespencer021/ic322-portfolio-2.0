# Review Questions

## R5. What is the “count to infinity” problem in distance vector routing?
The "count to infinity" problem is seen when the cost between two routers increases as seen in image **b.** of Figure 5.7 below (between *x* and *y* changed from 4 to 60). In this example, before the change, router *z* knows that it can send to *x* via *y* with a cost of 5. Router *y* sees the increase and updates its distances. Since *z* in the past has told *y* that it can reach *x* with a cost of 5, *y* calculates its new total cost to *x* to be 6 (1 to *z* and then 5 from *z*) because this is still less than 60. It sends an update to *z* that states that its distance to *x* is 6 instead of the previous 4. Now *z* will update its cost to *x* by comparing the direct router (50) with the route via *y* which in this case it believes to be 7 (1 to *y* then 6 from *y* to *x*). Then *y* would update again and say 8, then *z* would say 9, and so on until the number increases beyond the 50 that it costs from *z* to *x* directly. During this entire process, neither *z* nor *y* can send anything to *x* because they will just send to each other because they think that the path via the other router router is faster than sending it directly. The fact that *y* to *x* increased to 60 traveled to *z* very slowly here. This is called the "count to infinity" problem because if you imagine the case when instead of increasing from 4 to 60, it increased from 4 to 10,000, it would take an extremely long time before anything could be sent to router *x* effectively.

![Figure 5.7](Week09/Figure5.7.png)

## R8. True or false: When an OSPF router sends its link state information, it is sent only to those nodes directly attached neighbors. Explain.
False: An OSPF router sends its link state information to all routers within the area, not only to its directly attached neighbors. It sends out a broadcast when it updates this information and every half hour to make sure that all the routers are as up-to-date as possible.

## R9. What is meant by an area in an OSPF autonomous system? Why was the concept of an area introduced?
Each autonomous system (AS) consists of a group of routers that are under than same administrative control. Among these, every router runs the same algorithm and have information about all the other routers in the AS. There are border routers that are resonsible for routing packets within a given area. There is a backbone area which is responsible for routing traffic between different areas, and the backbone area contains all the area border routers within the AS. To route between areas, a packet is routed to an area border router, then through the backbone to the area border router of its destination area, then to its final destination.

## P3. Consider the following network. With the indicated link costs, use Dijkstra’s shortest-path algorithm to compute the shortest path from x to all network nodes. Show how the algorithm works by computing a table similar to Table 5.1.
![P3 Image](Week09/p3.png)

| Step | N' | D(*t*),p(*t*) | D(*u*),p(*u*) | D(*v*),p(*v*) | D(*w*),p(*w*) | D(*y*),p(*y*) | D(*z*),p(*z*) | 
| :-: | - | :-: | :-: | :-: | :-:| :-: | :-: |
| 0 | x | inf | <ins>3,x</ins> | 6,x | inf | 6,x | 8,x |
| 1 | x,v,w | 7,v | - | <ins>6,x</ins> | 6,v | 6,x | 8,x |
| 2 | x,v,w,u | 7,v | - | - | <ins>6,v</ins> | 6,x | 8,x |
| 3 | x,v,w,u,y | 7,v | - | - | - | <ins>6,x</ins> | 8,x |
| 4 | x,v,w,u,y,t | <ins>7,v</ins> | - | - | - | - | 8,x |
| 5 | x,v,w,u,y,t,z | - | - | - | - | - | <ins>8,x</ins> |

- N.B. *inf* = *infinity*

## P4. Consider the network shown in Problem P3. Using Dijkstra’s algorithm, and showing your work using a table similar to Table 5.1, do the following:
**a.** Compute the shortest path from *v* to all network nodes.

| Step | N' | D(*t*),p(*t*) | D(*u*),p(*u*) | D(*w*),p(*w*) | D(*x*),p(*x*) | D(*y*),p(*y*) | D(*z*),p(*z*) | 
| :-: | - | :-: | :-: | :-: | :-:| :-: | :-: |
| 0 | v | 4,v | 3,v | 4,v | <ins>3,v</ins> | 8,v | inf |
| 1 | v,x | 4,v | <ins>3,v</ins> | 4,v | - | 8,v | 11,x |
| 2 | v,x,u | 4,v | - | <ins>4,v</ins> | - | 8,v | 11,x |
| 3 | v,x,u,w | <ins>4,v</ins> | - | - | - | 8,v | 11,x |
| 4 | v,x,u,w,t | - | - | - | - | <ins>8,v</ins> | 11,x |
| 5 | v,x,u,w,t,y | - | - | - | - | - | <ins>11,x</ins> |

- N.B. *inf* = *infinity*

**b.** Compute the shortest path from *y* to all network nodes.

| Step | N' | D(*t*),p(*t*) | D(*u*),p(*u*) | D(*v*),p(*v*) | D(*w*),p(*w*) | D(*x*),p(*x*) | D(*z*),p(*z*) | 
| :-: | - | :-: | :-: | :-: | :-:| :-: | :-: |
| 0 | y | 7,y | inf | 8,y | inf | <ins>6,y</ins> | 12,y |
| 1 | y,x | <ins>7,y</ins> | inf | 8,y | 12,x | - | 12,y |
| 2 | y,x,t | - | 9,t | <ins>8,y</ins> | 12,x | - | 12,y |
| 3 | y,x,t,v | - | <ins>9,t</ins> | - | 12,x | - | 12,y |
| 4 | y,x,t,v,u | - | - | - | <ins>12,x</ins> | - | 12,y |
| 5 | y,x,t,v,u,w | - | - | - | - | - | <ins>12,y</ins> |

- N.B. *inf* = *infinity*

**c.** Compute the shortest path from *w* to all network nodes.

| Step | N'          | D(*t*),p(*t*) | D(*u*),p(*u*) | D(*v*),p(*v*) | D(*x*),p(*x*) | D(*y*),p(*y*)  | D(*z*),p(*z*)  | 
| :--: | ----------- | :-----------: | :-----------: | :-----------: | :-----------: | :------------: | :------------: |
| 0    | w           | inf           | <ins>3,w</ins>| 4,w           | 6,w           | inf            | inf            |
| 1    | w,u         | 5,u           | -             | <ins>4,w</ins>| 6,w           | inf            | inf            |
| 2    | w,u,v       | <ins>5,u</ins>| -             | -             | 6,w           | 12,v           | inf            |
| 3    | w,u,v,t     | -             | -             | -             | <ins>6,w</ins>| 12,v           | inf            |
| 4    | w,u,v,t,x   | -             | -             | -             | -             | <ins>12,y</ins>| 14,x           |
| 5    | w,u,v,t,x,y | -             | -             | -             | -             | -              | <ins>14,x</ins>|

- N.B. *inf* = *infinity*

## P5. Consider the network shown below. Assume that each node initially knows the costs to each of its neighbors. Consider the distance-vector algorithm and show the distance table entries at node *z*.
|   | z | x | v | u | y |
| - | - | - | - | - | - |
| z | 0 | 2 | 3 | 5 | 7 |
| x | 2 | 0 | 1 | 3 | 5 |
| v | 3 | 1 | 0 | 2 | 5 |

## P11. Consider Figure 5.7. Suppose there is another router w, connected to router y and z. The costs of all links are given as follows: c(x,y) = 4, c(x,z) = 50, c(y,w) = 1, c(z,w) = 1, c(y,z) = 3. Suppose that poisoned reverse is used in the distance-vector routing algorithm.
![Figure 5.7](Figure5.7.png)
### a. When the distance vector routing is stabilized, router *w*, *y*, and *z* inform their distances to x to each other. What distance values do they tell each other?
- *w* tells *y* infinity
- *w* tells *z* 5
- *y* tells *w* 4
- *y* tells *z* 4
- *z* tells *w* 7
- *z* tells *y* infinity

### b. Now suppose that the link cost between *x* and *y* increases to 60. Will there be a count-to-infinity problem even if poisoned reverse is used? Why or why not? If there is a count-to-infinity problem, then how many iterations are needed for the distance-vector routing to reach a stable state again? Justify your answer.
In this case there will not be a count-to-infinity problem. *y* will see that its direct distance to *x* is now 60 instead of 4, and because *z* told *y* that its distance to *x* is infinity, *y* will update its shortest route to *x* to be 60 and send that to *z*. *z* will then calculate that its shortest route is now directly to *x* becasue 50 is less than 61. *z* will tell *y* this new distance, and *y* will update its shortest distance to *x* to be 51. *y* will then tell *z* that its distance to *x* is infinity because its shortest is through *z*.

### c. How do you modify c(y,z) such that there is no count-to-infinity problem at all if c(y,x) changes from 4 to 60?
If c(*y*,*z*) is changed to 60, then *y*'s shortest path to *x* will still be direct. Then if c(*y*,*x*) increases, the shortest route from *z* to *x* will be direct and there will be no count-to-infinity problem because *y* will update its shortest route to *x* to be 60, so the increase will be communicated to *z* very quickly.