# Review Questions

## R1. What is the difference between a host and an end system. List several different types of end systems

End systems are devices that are connected to the edge of the internet. Mobile devices, servers, and desktop devices are all end systems. Hosts are another name for end systems becasue they host things on them. The term host and end system are interchangeable according to the textbook. Both clients and servers are types of hosts. Clients are typically devices that we and own whereas servers are generally more powerful devices that provide services for clients such as webpages and video streams.

## R4. List four access technologies. Classify each one as home access, enterprise access, or wide-area wireless access.

1. Cable: home access
2. Ethernet: enterprise & home access
3. WiFi: enterprise & home access
4. Mobile devices: wide-area wireless

## R11. Suppose there is exactly one packet switch between a sending host and a receiving host. The transmission rates between the sending host and the switch and between the switch and the receiving host are R1 and R2, respectively. Assuming that the switch uses store-and-forward packet switching, what is the total end-to-end delay to send a packet of length L? (Ignore queuing, propagation delay, and processing delay.)

The delay between the sending host and the switch is L/R1, and the delay between the switch adn the receiving host is L/R2, so the total delay is L/R1 + L/R2

## R12. What advantage does a circuit-switched network have over a packet-switched network? What advantages does TDM have over FDM in a circuit-switched network?

A circuit-switched network, once a communication session is setup, has the resources for the connection reserved until the communication session is over, thus it will never have to queue as a packet-switched network may have to do. 

Time Division Multiplexing (TDM) has the advantage of providing a sender-receiver pair with the full bandwith during their specific time interval. It also is easier to implement than Frequency Division Multiplexing (FDM).

## R13. Suppose users share a 2 Mbps link. Also suppose each user transmits continuously at 1 Mbps when transmitting, but each user transmits only 20 percent of the time. (See the discussion of statistical multiplexing in Section 1.3.)

**a. When circuit switching is used, how many users can be supported?**

In this case, two users can be supported because each of them uses 1 Mbps and there are 2 Mbps available.

**b. For the remainder of this problem, suppose packet switching is used. Why will there be essentially no queuing delay before the link if two or fewer users transmit at the same time? Why will there be a queuing delay if three users transmit at the same time?**

If two users were to transmit at the same time, both transmissions could go through without any issue because the link can support 2 Mbps. If three users were to transmit at the same time, one would have a queuing delay because the full bandwidth would be used by the other two and the third would have to wait for those transmissions to go through before it could.

**c. Find the probability that a given user is transmitting.**

The probability that a user is transmitting at any given time is 20%.

**d. Suppose now there are three users. Find the probability that at any given time, all three users are transmitting simultaneously. Find the fraction of time during which the queue grows.**

The probability that all three users are transmitting at the same time is 20% * 20% * 20% which is 0.8%. The fraction of time that the queue will grow is the fraction of time that all three transmit at the same time which is 0.8% which, in fraction form is 1/125.

## R14. Why will two ISPs at the same level of the hierarchy often peer with each other? How does an IXP earn money?

Two ISPs at the same level will peer with each other because that means that they do not have to go to an ISP that is higher in the hierarchy and they can remove the "middle man". This also removes the need to pay the higher level ISP, so it saves money.

The answer to how an IXP earns money was not in the book, so the answer was on [this website](https://packetpushers.net/htirw-internet-exchange-points/#:~:text=IXPs%20sell%20their%20services%20based,it's%20the%20same%20basic%20concept.) which I found from LCDR Downs' example portfolio. According to that website, IXPs sell ports for accessing the space that they created to help ISPs peer.

## R18. How long does it take a packet of length 1,000 bytes to propagate over a link of distance 2,500 km, propagation speed 2.5 * 10^8 m/s, and transmission rate 2 Mbps? More generally, how long does it take a packet of length L to propagate over a link of distance d, propagation speed s, and transmission rate R bps? Does this delay depend on packet length? Does this delay depend on transmission rate?

The total delay is the sum of three parts: queuing delay, transmission delay, and propogation delay.

Queuing delay is the time spent waiting for other packets ahead before a given packet can be handled. There is no queuing delay here because there is only one packet.

Transmission delay is how long it takes for the bits to actually be put onto the wire. This is mathematically represented as $L \over R$. In this case the queuing delay is $8000 bits \over 2 Mbps$ which is 0.004 seconds.

Propogation delay is how long it takes for the bits to travel over the length of the wire to their destination. This is represented mathematically as $d \over s$. In this case the propogation delay is $2500 km \over 2.5 * 10^8 m/s $ which is 0.01 seconds.

The total time in this case is 0.004s + 0.01s = 0.014s. The delay will change dependent on packet length because longer packets will have a longer transmission delay.

## R19. Suppose Host A wants to send a large file to Host B. The path from Host A to Host B has three links, of rates R1=500kbps, R2=2 Mbps, and R3=1 Mbps

**a. Assuming no other traffic in the network, what is the throughput for the file transfer?**

The throughput for the entire transfer will be limited by the link with the slowest rate, which is R1 = 500kbps.

**b. Suppose the file is 4 million bytes. Dividing the file size by the throughput, roughly how long will it take to transfer the file to Host B?**

File size divided by throughput is $4,000,000 bytes \over 500 kbps$ = 8 seconds.

**c. Repeat (a) and (b), but now with R2 reduced to 100 kbps.**

- a. (part 2) The new throughput is 100kbps.
- b. (part 2) $4,000,000 bytes \over 100 kbps$ = 40 seconds.
