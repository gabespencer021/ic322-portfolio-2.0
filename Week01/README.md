# Week 1: The Internet

*Gabriel Spencer | Fall 2023*

This week introduced us to a basic understanding of the internet and its general structure. We also looked at packets and delays.

* [Review Questions](Week01/ReviewQs.md)
* [Learning Goals](Week01/LearningGoals.md)
* [Wireshark Intro](Week01/WiresharkIntro.md)
* Back to [Overview](../README.md)
