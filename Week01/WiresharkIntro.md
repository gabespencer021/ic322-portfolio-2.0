# Wireshark Intro

## Introduction

This lab was a very cursory introduction to Wireshark and sniffing packets. I learned the basics of what Wireshark does and how to use it.

## Collaboration

The only collaboration that I used was looking at LCDR Downs' example Portfolio to help me understand what his expectations were and to make sure that I was doing it properly.

## Process

I used the lab instructions linked [here](Week01/Wireshark_Intro_v8.1.docx) to complete this lab. They are self-explanatory and I did not do anything extra that the instructions did not specify.

## Lab Questions

**1. Which of the folloowing protocols are shown as appearing in your trace file: TCP, QUIC, HTTP, DNS, UDP, TLSv1.2?**

TCP, HTTP, DNS, UDP, and TLSv1.2 all are shown as appearing in the trace file.

**2. How long did it take from when the HTTP GET message was sent until the HTTP OK reply was received?**

It took 0.0209 seconds.

**3. What is the Internet address of the gaia.cs.umass.edu (also known as www-net.cs.umass.edu)? What is the Internet address of your computer or (if you are using the trace file) the computer that sent the HTTP GET message?**

The IP address of gaia.cs.umass.edu is 128.119.245.12. The IP address of my computer is 10.17.19.110.

**4. Expand the information on the HTTP message in the Wireshark “Details of selected packet” window (see Figure 3 above) so you can see the fields in the HTTP GET request message. What type of Web browser issued the HTTP request?**

Wireshark shows this:
User-Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/116.0.0.0 Safari/537.36\r\n

I do not know if it was Chrome or Safari. 

**5. Expand the information on the Transmission Control Protocol for this packet in the Wireshark “Details of selected packet” window (see Figure 3 in the lab writeup) so you can see the fields in the TCP segment carrying the HTTP message. What is the destination port number (the number following “Dest Port:” for the TCP segment containing the HTTP request) to which this HTTP request is being sent?**

The destination port number is 80.

**6. Print the two HTTP messages (GET and OK) referred to in question 2 above.**

View the GET message here:<br><br>
![GET Message](/Week01/GET.jpg)

View the OK message here:<br><br>
![OK Message](/Week01/OK.jpg)