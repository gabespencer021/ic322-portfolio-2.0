# Learning Goals

## I can explain the role that the network core plays vs the network edge.

The network core is made up of ISPs and all the other networking equipment used to move information from one place to another. The network edge is made up of hosts (end systems). The machines on the network edge are te things that use the internet while the network core is responsible for transmitting the data across the internet from its source to where it needs to go.

## I can compare different physical technologies used in access networks, including dial-up, DSL, cable, fiber, and wireless.

Dial-up internet was a way for someone to access the internet through their phone. They would dial the number for the server, and their computer and the server would "talk" to each other with sounds over the phone. There was no network installation required here besides what was already in place for phones.

DSL (Digital Subscriber Line) was a way that allowed for internet access and phone calls at the same time. It split the phone line spectrum into a voice channel and a data channel. At this point you could talk on the phone and use the internet at the same time without interference because the phone companies were able to separate the two and send the one set of information to the other end of the phone line and the other data into the network core.

Cable internet used television cables to carry internet as well They did this by splitting the spectrum into video (for TV) and data (for internet) channels, similarly to how DSL works.

Fiber is when the companies' fiber connections (which used to end and change to cables when it got near your house) are extended all the way to people's houses.

Wireless internet can have a couple different ways that it is implemented. Wifi is used to connect hosts in a small area to the internet via a router. 3G/4G/5G connect hosts to the network core directly, typically via cell towers which are strategically placed all over so that a person can be in range of one most places that they go.

## I can use queuing, transmission, and propagation delays to calculate total packet delay, I can describe the underlying causes for these delays, and I can propose ways to improve packet delay on a network.

**Queuing delay:** This is the delay that happens because packets have to wait for packets that were previously sent to be processed or transmitted. This can be improved by improving one of the other delays so that packets do not have to wait as long for their "turn". This is calculated based on the other delays and how long a packet will have to wait on other packets.

**Transmission delay:** This is how long it takes to put packets onto a wire. It is based on how fast the bits can be put onto a wire so that they can be sent. This can be improved by getting better cables. This is calculated by dividing transmission rates by packet size: $Rate \over Size$.

**Propagation delay:** This is how long it takes for packets to travel along a wire between hosts. The best way to improve this is to shorten the distance that the packets have to travel. This is calculated by dividing distance to travel by speed of travel: $Distance \over Speed$.

## I can describe the differences between packet-switched networks and circuit-switched networks.

Circuit-switched networks have the entire bandwidth of a circuit open for the duration of the connection. This is not good when the entire bandwidth is not being used because that bandwidth is not being used for anything at all. 

In packet-switched networks, people's messages and data are broken up into packets, and multiple different users' data are all sent on the same circuit and put back together into a coherent message on the other side. This means that there could be some more queuing delays than before because the full bandwidth may be used, but it also means that much less bandwidth is wasted because it is not just one user per circuit.

## I can describe how to create multiple channels in a single medium using FDM and TDM.

**FDM (Frequency Division Multiplexing)** is when bandwidth is divided into different frequency ranges and this assigns different types of data to different bandwidth.

**TDM (Time Division Multiplexing)** is when the bandwidth is fully devoted to a single thing for a short amount of time, then moves on to the next thing. There is an assigned order and so each thing gets the full bandwidth for its small sections of allotted time. This happens again and again in a cycle, giving different hosts, one after the other, the full bandwidth. Ideally this provides an appearance of a constant connection to the user.

## I can describe the hierarchy of ISPs and how ISPs at different or similar levels interact.

There are three types of ISPs: local, regional, and tier 1.

**Local ISPs:** These ISPs provide internet to users that are not ISPs. Another name for these is access ISPs.

**Regional ISPs:** These ISPs will sell internet to both lower-level ISPs (local) and to users directly. 

**Tier 1 ISPs:** These ISPs form the bulk of the internet. The provide internet access to any other ISP, such as local or regional. The local and regional ISPs pay them for access to the internet.

## I can explain how encapsulation is used to implement the layered model of the Internet.

Users use layer 5 to send something to another layer 5 somewhere else. To get there, the layer 5 must pass the information down to layer 4. Layer 4 then adds a header and passes it to layer 3. Layer 3 adds its own header and passes to layer 2, and then layer 2 adds its header and passes  it to layer 1 to be sent to where it has to go. Once it gets to its destination, it is passed back up the layers with each layer removing its header and checking to make sure that the data that is sent goes to the right place. If it is in the wrong place, the layer adds its own header again and passes it back down to be sent to the right place. Each layer only knows what it has to know. The upper layers know nothing about the headers that the layers below add to the data being sent, and the layers that add headers do not know or care what the data being given to them is as long as they do the right thing with their header.