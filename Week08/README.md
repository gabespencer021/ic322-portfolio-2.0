# Week 8: More Network Layer: More Data Plane

*Gabriel Spencer | Fall 2023*

This week

* [Review Questions](Week08/ReviewQs.md)
* [Learning Goals](Week08/LearningGoals.md)
* [Lab: Beta Tester](Week08/Lab.md)
* [Back to Overview](../README.md)
