# Protocol Pioneer Chapters 3 & 4

## Introduction

This lab was chapters 3 and 4 of the Protocol Pioneer game. It is a basic networking game that allows you to basically encode routers to forward messages properly and ideally make the entire small network function properly.

## Collaboration

I did not collaborate with anyone for this lab.

## Process

I followed the instructions on the [course website](https://ic322.com/#/assignments/week07/protocol-pioneer/) for this lab.

## Questions

### 1. How did you solve the Chapters? Please copy and paste your winning strategy(s), and also explain it in English.
**Chapter 3:**
>The strategy for Chapter 3 was to break up the message into the destination, command, and value. I then set up 'if' statements to check for which drone each message arrived at, and depending on which drone it was at and where its destination was, the drone sent the same message in the proper direction. If the message had arrived at its destination, then I would have the drone run a different command based on what command and value were in the message. See the code below:
```
def process_strategy(self):
    
    while(self.message_queue):
        m = self.message_queue.pop()
        # You can use the `self.id` value to create different logic for different drones.
        print(f"--- Drone {self.id}: Msg on interface {m.interface} ---\n{m.text}\n------------------")
        mess = m.text.split("\n")
        dest = mess[0].split(":")[1]
        cmd = mess[1].split(":")[1]
        val = mess[2].split(":")[1]

        if dest == self.id: # found destination
            if cmd == "start_emit":
                self.start_emit(val)
            elif cmd == "focus":
                self.focus(val)
            elif cmd == "return_results":
                self.return_results(val)

        elif self.id == '1':
            if dest == '3':
                self.send_message(m.text,'N')
            else:
                self.send_message(m.text,'E')
        elif self.id == '2':
            if dest == '1':
                self.send_message(m.text,'W')
            elif dest == '5':
                self.send_message(m.text,'S')
            else:
                self.send_message(m.text,'N')
        elif self.id == '3':
            if dest == '1':
                self.send_message(m.text,'S')
            else:
                self.send_message(m.text,'E')
        elif self.id == '4':
            if dest == '6':
                self.send_message(m.text,'N')
            elif dest == '3':
                self.send_message(m.text,'W')
            else:
                self.send_message(m.text,'S')

# Remember, you can set `wait_for_user` to be True if you want the game to pause every tick.
s = Chapter3(process_strategy, wait_for_user=False)
s.run()
```

**Chapter 4:**
>The strategy for Chapter 4 was separate depending on whether it was a drone or a scanner that was receiving the message. The drones would split the message into teh source, destination, command, and value. It would then check what the destination was, and based on the destination and which drone it was currently at, the original message would be sent on in a particular direction. The scanners would receive the message and parse it the same as the drones. Then it would double check to make sure that the message had arrived at the proper destination. If it had not, it would resend the original message towards its proper destination. If it had arrived in the right place, the scanner would use the value and command to calculate a result. It would then make a response message with the source and destination from the original message switched, and with the result that it calculated. It would then send this message back to the source of the original message. See the code below:
```
def drone_strategy(self):
    """Drones are responsible for routing messages."""
    while(self.message_queue):
        m = self.message_queue.pop()
        print(f"--- Drone {self.id}: Msg on interface {m.interface} ---\n{m.text}\n------------------")
        mess = m.text.split("\n")
        src  = mess[0].split(":")[1]
        dest = mess[1].split(":")[1]
        cmd  = mess[2].split(":")[1]
        val  = mess[3].split(":")[1]

        if dest == '2.2':
            if self.id == '3.4':
                self.send_message(m.text,'N')
            elif self.id == '3.1' or self.id == '3.2' or self.id == '3.3':
                self.send_message(m.text,'E')
            else:
                self.send_message(m.text,'S')
        if dest == '2.1':
            if self.id == '3.1' or self.id == '3.2' or self.id == '3.3':
                self.send_message(m.text,'E')
            else:
                self.send_message(m.text,'N')
        if dest == '1.1': 
            if self.id == '3.1' or self.id == '3.7':
                self.send_message(m.text,'W')
            else:
                self.send_message(m.text,'N')
        if dest == '1.2':
            if self.id == '3.1':
                self.send_message(m.text,'S')
            elif self.id == '3.4' or self.id == '3.3':
                self.send_message(m.text,'N')
            else:
                self.send_message(m.text,'W')
        if dest == '1.3':
            if self.id == '3.4' or self.id == '3.5':
                self.send_message(m.text,'W')
            else:
                self.send_message(m.text,'S')

def scanner_strategy(self):
    """Scanners are responsible for receiving messages, parsing them, taking
    action, and responding with results."""
    while(self.message_queue):
        m = self.message_queue.pop()
        print(f"--- Scanner {self.id}: Msg on interface {m.interface} ---\n{m.text}\n------------------")
        mess = m.text.split("\n")
        src  = mess[0].split(":")[1]
        dest = mess[1].split(":")[1]
        cmd  = mess[2].split(":")[1]
        val  = mess[3].split(":")[1]
        result = ''
        response = ''

        # if it's not actually at the right place, send message so it will get there
        if dest != self.id:
            response = m.text

        elif dest == self.id:
            if cmd == "Boot":
                result = self.boot(val)
            elif cmd == "Aim":
                result = self.aim(val)
            elif cmd == "Scan":
                result = self.scan(val)

            response = "Source:" + dest + "\nDest:" + src + "\nCommand:Result\nValue:" + str(result)

        if self.id == '2.1':
            self.send_message(response,'S')
        else:
            self.send_message(response,'N')

# Set animation_frames to a lower number to speed up the simulation.
s = Chapter4(drone_strategy, scanner_strategy, animation_frames=10, wait_for_user=False)
s.run()
```

### 2. Include a section on Beta Testing Notes.

- **a.** List any bugs you find. When you list the bugs, try to specify the exact conditions that causes them so I can reproduce them.
        The only bug that I encountered did not actually cause any issues in the running of the program, but it did pop up every time that I ran all the cells. It had a red error box that said something about an "invalid escape sequence", but the error disappeared, so I cannot include a screenshot or anything.
- **b.** Was there anything that you and others were consistently confused about? What would you do to solve that confusion?
        I was a little bit confused at the very beginning of chapter one about what I was supposed to do, but it was largely because the directions were correct but I did not follow them precisely because I felt like I was missing something that I was supposed to do. I just did not trust the early instructions enough.
- **c.** Was there too much narrative? Not enough narrative?
        I thought there was a good amount of narrative and it allowed for a bit of a story but did not waste a ton of time with details that were just keeping me from finishing the assignment. I like that there is a story, but that the story does not dominate too much so taht I can get to the assignment part.
- **d** Was it fun?
        I enjoyed playing these chapters, and I think it's because I enjoy coding and logic. I think the game is actually fun and I much prefer it to a Wireshark lab.
- **e.** Really, just let me know how I can improve the game.
        During chapters 3 and 4, the game started to feel a little bit like an exercise in using 'if', 'elif', and 'else' statements, and like each chapter was just a slightly more complicated series of these conditionals than the chapter before. However, considering that this is only the introduction to the basics of the game, I think it will probably be better in future chapters, so this feedback is probably pretty pointless.