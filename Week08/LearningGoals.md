# Learning Goals

## I can explain the problem that NAT solves, and how it solves that problem.

The issue of IP addresses is that there are too many hosts that have to share too few IP addresses, so hosts cannot be individually assigned their unique IP address that no other host will share. NAT solves this because when a packet goes through a router, the router translates the source IP address of that router to be its own IP address and assigns it a random port number. It then stores this information in a table alongside the original source iP address and port number that it received. This means that every packet that it forwards will be seen on the outside as coming from the same source (the router's IP), and this will limit the number of IP addresses that are present out on the public Internet. If this happens for all routers, then different hosts on different subnets can have the same IP address and there will be no issue because their respective hosts translated their IPs before forwarding the packets. When a packet is getting sent back to the host, it arrives at the router which has the IP address and port number of the destination, and the router looks in its NAT table and matches the IP and port with its original source IP and port. It then changes the packet's destination to be the original source IP address and port number, and it forwards the packet. This whole process keeps private IP addresses local and keeps them from getting confused for each other out on the public Internet, or from causing there to be an insufficient number of IP addresses.

## I can explain important differences between IPv4 and IPv6.

The most important changes from IPv4 to IPv6 are the following changes to the datagram format:
- *Expanded addressing capabilities*: the size of the IP address is changed from 32 to 128 bits. This means there would never be a shortage of IP addresses.
- *A streamlined 40-byte header*: Some of the IPv4 fields are dropped or made optional, so the 40 byte header can actually be processed faster by routers.
- *Flow labeling*: certain types of requests are treated as a flow, such as video and audio. The sender requests special handling for packets that are part of a flow.

The following fields are defined in IPv6:
- *Version*: 4-bit field identifies the IP version number (IPv6 has a 6 in this field)
- *Traffic class*: 8-bit traffic class field can give priority to certain datagrams within a flow, or to datagrams from certain applications
- *Flow label*: 20-bit field used to identify a flow of datagrams
- *Payload length*: 16-bit value treated as an unsigned integer givign the number of bytes following the header in the datagram
- *Next header*: identifies the protocol where the contents of this datagram will be delivered, e.g. UDP or TCP
- *Hop limit*: contents of this decreased by one each router that forwards the datagram. If it reaches zero, a router will discard teh packet
- *Source and destination addresses*: formatted differently than IPv4
- *Data*: payload portion of the datagram. At the destination, the payload will be removed from the datagram and passed to the protocol specified in the header field

The following fields are present in IPv4 but not in IPv6:
- *Fragmentation/reassembly*: IPv6 does not allow for fragmentation and reassembly. Only the source and destination can do these things. If a packet is too big, the router just drops it and sends a "Packet Too Big" ICMP error message to the sender. Because fragmentation and reassembly is so time-consuming, eliminating these operations considerably speeds up the router's operations for IPv6.
- *Header checksum*: Because the transport layer and link layer already perform checksum operations, that is ocnsidered redundant enough, so the checksum operation is removed in IPv6.
- *Options*: The options field is not totally eliminated in IPv6, but just moved to another location.

## I can explain how IPv6 datagrams can be sent over networks that only support IPv4.

When an IPv6 datagram is being sent between two IPv6-compatible routers across an IPv4 network, the network makes a "tunnel" between the IPv6 routers for the datagram to go through. When the datagram reaches an IPv4 router, the *entire* IPv4 datagram is put into the data (payload) field of an IPv4 datagram which is addressed to the IPv6 router at the other end of the tunnel. This datagram is then treated like a regular IPv4 datagram by all the IPv4 routers, completely unaware that it has an IPv6 datagram within it. When the datagram reaches its new destination (the IPv6 router), it determines that its payload is an IPv6 datagram and extracts it, and treats it exactly as it would if it received the datagram directly from another IPv6 router.
![Tunnel Image](Week08/ipv6tunnel.png)