# Review Questions

## R23. Visit a host that uses DHCP to obtain its IP address, network mask, default router, and IP address of its local DNS server. List these values.
- IP Address: 172.30.154.56
- Network Mask: 255.255.192.0.0
- Default Router: 172.30.128.1
- DNS IP Address: 172.21.192.11

## R29. What is a private network address? Should a datagram with a private network address ever be present in the larger public Internet? Explain.
A private network address is an IP address assigned to a host on a given private network. That IP address is present within that subnet, and outside of it, it is unknown/unreachable. Within a given subnet, the private addresses are used to send packets to and from hosts, but once the addresses go outside of the subnet (through the router), then they are translated using a NAT table and are not the same anymore. For this reason, private network addresses should not be present in the larger public Internet. Multiple subnets that are separate and are themselves contained within different subnets can use the same group of addresses, so the existence of private network addresses in the larger public Internet would pose an issue because the same IP address could refer to multiple different things.

## R26. Suppose you purchase a wireless router and connect it to your cable modem. Also suppose that your ISP dynamically assigns your connected device (that is, your wireless router) one IP address. Also suppose that you have five PCs at home that use 802.11 to wirelessly connect to your wireless router. How are IP addresses assigned to the five PCs? Does the wireless router use NAT? Why or why not?
The router will assign addresses to the five different PCs using DHCP when they connect. The router will still use NAT because the private IP addresses need to stay on the private network, so if packets are sent past the router, they need to have the addresses translated to keep their IP addresses from clogging up the internet. If all the IP addresses of all the hosts were on the same internet, there probably wouldn't be any good way to distinguish. They are grouped by routers which use address translation to keep the private IPs on private networks and off of the public Internet.

## P15. Consider the topology shown in Figure 4.20. Denote the three subnets with hosts (starting clockwise at 12:00) as Networks A, B, and C. Denote the subnets without hosts as Networks D, E, and F.

### a. Assign network addresses to each of these six subnets, with the following constraints: All addresses must be allocated from 214.97.254/23; Subnet A should have enough addresses to support 250 interfaces; Subnet B should have enough addresses to support 120 interfaces; and Subnet C should have enough addresses to support 120 interfaces. Of course, subnets D, E and F should each be able to support two interfaces. For each subnet, the assignment should take the form a.b.c.d/x or a.b.c.d/x – e.f.g.h/y.

| Subnet | Number of Interfaces | Network Addresses | Last 9 Bits Range |
| :--: | ---------------------- | ----------------- | ----------------- |
| A | 250 | 214.97.254/24 | 0 00000000 -> 0 111111111 |
| B | 120 | 214.97.255.0/26<br>214.97.255.64/27<br>214.97.255.96/28<br>214.97.255.112/29 | 1 00000000 -> 1 01110111 |
| C | 120 | 214.97.255.128/25 | 1 10000000 -> 1 11111111 |
| D | 2 | 214.97.255.120/31 | 1 01111000 -> 1 01111001 |
| E | 2 | 214.97.255.122/31 | 1 01111010 -> 1 01111011 |
| F | 2 | 214.97.255.124/31 | 1 01111100 -> 1 01111101 |

### b. Using your answer to part (a), provide the forwarding tables (using longest prefix matching) for each of the three routers.

| Prefix<br>(from last 9 bits) | Output |
| ------ | :----: |
| 0 | A |
| 100 | B |
| 1010 | B |
| 10110 | B |
| 101110 | B |
| 11 | C |
| 10111100 | D |
| 10111101 | E |
| 10111110 | F |

## P16. Use the whois service at the American Registry for Internet Numbers (http://www.arin.net/whois) to determine the IP address blocks for three universities. Can the whois services be used to determine with certainty the geographical location of a specific IP address? Use www.maxmind.com to determine the locations of the Web servers at each of these universities.

| University Name | IP address range | CIDR | Location |
| --------------- | ---------------- | ---- | -------- |
| Missouri University | 151.101.0.0 - 151.101.255.255 | 151.101.0.0/16 | United States, North America |
| University of Kansas | 129.237.0.0 - 129.237.255.255 | 129.237.0.0/16 | Lawrence, KS |
| University of Missouri Kansas City | 134.193.0.0 - 134.193.255.255 | 134.193.0.0/16 | Rolla, MO |

You cannot use the whois services to determine with certainty the geographical location of a specific IP address as seen by the results from Missouri University when trying to determine its location with `www.maxmind.com`. Sometimes you can determine it, but sometimes you will not get all the information, like what happened when I searched `www.maxmind.com` for MU's IP address and it just gave United States, North America.

## P18. Consider the network setup in Figure 4.25. Suppose that the ISP instead assigns the router the address 24.34.112.235 and that the network address of the home network is 192.168.1/24.
<!-- ![p18](p18.png) -->
### a. Assign addresses to all interfaces in the home network.

- Host A: 192.168.1.1
- Host B: 192.168.1.2
- Host C: 192.168.1.3

### b. Suppose each host has two ongoing TCP connections, all to port 80 at host 128.119.40.86. Provide the six corresponding entries in the NAT translation table.

| WAN | LAN |
| :-: | :-: |
| 24.34.112.235/4001 | 192.168.1.1/4050 |
| 24.34.112.235/5002 | 192.168.1.1/4051 |
| 24.34.112.235/5001 | 192.168.1.2/5050 |
| 24.34.112.235/5002 | 192.168.1.2/5051 |
| 24.34.112.235/6001 | 192.168.1.3/6050 |
| 24.34.112.235/6002 | 192.168.1.3/6051 |

## P19. Suppose you are interested in detecting the number of hosts behind a NAT. You observe that the IP layer stamps an identification number sequentially on each IP packet. The identification number of the first IP packet generated by a host is a random number, and the identification numbers of the subsequent IP packets are sequentially assigned. Assume all IP packets generated by hosts behind the NAT are sent to the outside world.
### a. Based on this observation, and assuming you can sniff all packets sent by the NAT to the outside, can you outline a simple technique that detects the number of unique hosts behind a NAT? Justify your answer.

Yes, you just have to track the sequence numbers of all the packets coming from the router, and then you can put them together in order, making however many groups you need. Each group of sequence numbers will represent one host.

### b. If the identification numbers are not sequentially assigned but randomly assigned, would your technique work? Justify your answer.

The technique would no longer work because you could not group the packets based on their sequence numbers, so you would have no idea which packets came from which hosts, or which ones came from the same host.