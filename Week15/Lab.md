# Deep Dive: Security Spelunker

## 1999 NASA Cyber Attack

### What happened
In 1999, at the age of 15, Jonathan James hacked into Department of Defense and NASA computers. He was also the first juvenile hacker to serve a prison sentence. For these two reasons, he earned himself a spot in the hacker hall of fame. James to PBS "The government didn't take too many measures for security on most of their comupters." He entered 13 computers at the Mashall Sapce Flight Center in Huntsville, Alabama. "While there, While there, he stole data and downloaded $1.7 million in NASA proprietary software used to support the International Space Station’s physical environment, including control of the temperature and humidity within the living quarters"[1]. After NASA discovered the attack, they were forced to shut down their computers for three weeks for repairs and to check the extent of the damage. The cost was estimated to be $41,000.

### Vulnerabilities/How did he do it
He bypassed the firewalls on a vulnerable server in Huntsville, Alabama and then escalated the privileges giving himself access to 13 different computers on the system. This server that he entered was a NASA server at the Marshall Space Flight Center where they develop and test rockets and communications systems for the ISS.

He also hacked the DoD by installing a backdoor on a vulnerable server he found in Dulles, Virginia. He then used his backdoor to sniff all the traffic on the network, to include login credentials and other sensitive information. This compromised server belonged to the DTRA (Defense Threat Reduction Agency), part of the DoD. Using this, he was able to download thousands of messages from email users working for the Pentagon. Jonathan James' attack was the first successful hack into an internal system on one of the Pentagon's external units.

### Who is Jonathan James?
<img src="JonathanJames.jpg" width="141" height="198">
<!-- ![Jonathan James](JonathanJames.jpg) -->

- Young Jonathan James - source: medium.com

At the time that he hacked NASA, Jonathan James was a 15 year old schoolboy. He learned everything that he knew about computers on his own through curiosity and exploration. His dad learned that he was good with computers when he came home from work to find that Jonathan had replaced the Windows OS on his computer with a Linux OS because he wanted to learn how it worked. Jonathan began his hacking career by hacking into his school's system to change his grades. When he was arrested, he explained in detail how he hacked the systems, helping the vulnerabilities to be patched. After the investigation, it was clear that he had not altered any of the files, data, passwords, etc. and he had not installed any viruses. The fact that he caused little to no damaged, combined with the fact that he was only 16, allowed for his sentence to be much lighter than it could have been otherwise. He received six months of house arrest and a ban from computer usage except for schoolwork. It did not go as well as it could have because the police caught him violating the terms of his house arrest, and then they did blood work on him which revealed some type of drug in his system. The court then sentenced him to six months in prison in a juvenile correction center in Alabama. After this, Jonathan told reporters that he was finished hacking because it just wasn't worth the hassle.

Later, after he had been clear of any accusations or suspicions of hacking since his detention, the Secret Service began to investigate him as a suspect in the massive TJX hack. This was a group of hackers led by Albert Gonzalez who committed a bunch of credit card breaches at multiple different companies. Jonathan James denied any involvement in this or any other hacking until his dying day. The investigations revealed nothing that connected him with the hacks. The name JJ, a nickname tied to the hacks and the reason he was suspected in the first place, turned out to belong to Steven Watts who often went by the pseudonym of Jim Jones.

Despite being cleared of all charges, Jonathan James was found dead in his home on May 18, 2008 with a self-inflicted gunshot to his head. In his suicide note, he wrote that he does not believe in the justice system anymore, and he still denied any connection with the TJX attacks.

### Methods for Mitigation
Some simple mitigation steps for preventing such attacks are as follows:
- Having more secure firewalls
- Not sending any login credentials without encryption or some other method of security
    - This hack shows some of the reason that things which are sent on the internet should be encrypted if you want privacy because you never know who is watching
- Not sending classified information or any other important information over email

### Pillars: Confidentiality vs. Integrity vs. Authentication
- Confidentiality: Confidentiality was not preserved here because he was able to access lots of information that he should not have been able to access. The lack of security of the firewalls and the lack of care when sharing sensitive information allowed Jonathan to access lots of information that should have remained confidential.
- Integrity: Integrity was preserved in this instance because although Jonathan access and downloaded lots of files and information, he never changed any of it, or at least there is no evidence that he did.
- Authentication: Authentication was violated because Jonathan accessed usernames and passwords that would allow him to appear as another user. He also could access a lot of information as if he were a different user.

### Ethical Implications
The fact that Jonathan could access so much important information was a huge risk. He could have spread that information to parties who would use it in a harmful way. He could have changed or deleted important data or software for things of international importance such as the ISS. He could have caused the loss of lives and extremely large amounts of money with the misuse of the information that he accessed. The ethical ramifications of him doing something harmful with his hack were enormous. But, he did not do anything particularly harmful with the data, so he falls into somewhat of gray area.

Professionals in the field must be sure to protect sensitive information well and ensure that no one who shouldn't have access to it has access. If there were losses of materials and money and life because of a hack like this, the responsibility falls on the person who should be ensuring that these vulnerabilities are not present. The professionals in the field have a very important job of ensuring they protect valuable informatio.

### Legal Considerations
Stealing information is, like stealing physical things, an action punishable by law. Accessing systems that you are not supposed to access is also illegal because if you access systems that are protected, you will have access to certain things that you should not. Harming or destroying information of someone else that is protected is also illegal, meaning that, in a case like that of Jonathan James, it was illegal for him to access what he accessed, but it would have been another crime on top of that for him to tamper at all with the information that he accessed.

### Professional Responsibilities
With regards to protecting sensitive information, computing professionals must be careful to ensure that all information that should be well-protected is well-protected, and that people who have access to this information also know what to avoid doing because it is often the person and not the computer which provides the greatest vulnerability to certain systems. Professionals must act so that people with harmful intent do not have any ability to access sensitive information. This will keep them from committing unethical acts which would cause lots of harm. It would also keep legal consequences away from all parties involved. Computing professionals should understand both the ethical and legal repercussions of a lack of security, especially of sensitive information. This understanding should drive them to ensure that they take their jobs seriously and ensure that they protect things to the proper extent, not cutting corners or taking any chances.

### Sources
[1]	G.Cohen, “Throwback Attack: A Florida teen hacks the Department of Defense and NASA,” industrialcybersecuritypulse.com,  Apr.  8, 2021. [Online].  Available: https://www.industrialcybersecuritypulse.com/throwback-attack-a-florida-teen-hacks-the-department-of-defense-and-nasa/  [Accessed: Apr. 25, 2022].

[2]	“Hacking Stories: Jonathan James – The teenager who hacked NASA for fun,” blackhatethicalhacking.com, Jul. 26, 2021. [Online]. Available: https://www.blackhatethicalhacking.com/articles/free-access/hacking-stories-jonathan-james-the-teenager-who-hacked-nasa-for-fun/ [Accessed: Apr. 25, 2022]


