# Learning Goals

## I can explain how two strangers are able to exchange secret keys in a public medium.
Public and private keys solve the issue of exchanging secret keys in a public medium. The keys are in pairs such that when a message is encrypted with a private key, it can be decrypted only with the corresponding public key, and if a message is encrypted with a public key, it can only be decrypted with the corresponding private key.

The steps:
- Bob and Alice each share their public keys with each other
- Bob encrypts his message to Alice with Alice's public key and sends it
- Alice receives the message and decrypts it with her private key to read the message
- Eve also receives the message, but cannot decrypt it because she only has access to Alice's public key

The same process reversed applies for Alice sending a message to Bob.

Symmetric keys:
- If Alice and Bob want to communicate with symmetric keys, they can through using public and private keys first.
- They have to create and share their keys as before
- They can then use the public and private key system to share a symmetric key with each other and Eve will not be able to see the symmetric key
- Now Alice and Bob can communicate with their symmetric key because that is simpler

## I can walk through the TLS handshake and explain why each step is necessary.
There are six steps to the TLS handshake:
1. What is it?
    - The client sends a list of cryptographic algorithms it supports, along with a client nonce.
    
    Why is it necessary?
    - The idea behind this step is to ensure that the encryption algorithm that is used is one that the client supports because otherwise encryption would be pointless and impossible

2. What is it?
    - The server chooses a symmetric algorithm and a public key algorithm from the list as well as an HMAC algorithm and HMAC keys. It sends these choices to the client along with a certificate and nonce.
    
    Why is it necessary?
    - The server and client must agree on both a symmetric and public key algorithm that they both can use, so the server picks from the list that the client sent. It also has to pick an HMAC algorithm and HMAC keys so that the server and client can verify their receipt of unedited messages. It sends its certificate with the nonce so that the client can verify that they are actually talking to the server and not some other entity. The nonce is used so that the same messages cannot be used in a connection replay attack.

3. What is it?
    - The client verifies the certificate and extracts the server's public key. It then generates a Pre-Master Secret (PMS) and encrypts it with the server's public key and sends the encrypted PMS to the server.
    
    Why is it necessary?
    - The client must verify the certificate to ensure that the message came from the server and not a malicious entity. It must generate a PMS because that will be used to create the real Master Secret (that the computers know). It encrypts it with the server's public key and sends the encrypted PMS to the server so that it can be sure that the server is the only one who can decrypt the PMS and use it to create keys.

4. What is it?
    - The client and server independently compute the Master Secret (MS) from the PMS and the nonces according to the key derivation function from the encryption algorithm specified earlier. The MS is then broken apart and used to create two encryption keys and two HMAC keys, one of each for each side of the communcation. When the chosen symmetric cypher employs CBC, two initialization vectors are created, one for each side of the connection. From now on, all the messages sent between the client and server will be encrypted and authenticated (with the HMAC).
    
    Why is it necessary?
    - The MS is calculated independently by the two because this is more secure than sending it from one to the other and because if they both calculate the same MS, it verifies the information that has already been exchanged. Two HMAC keys and two encryption keys are used so that one pair can be used to send from the client to the server and the other can be used to send from the server to the client. This is an extra layer of security. All messages will not be sent with the symmetric encryption that was agreed upon because it is now secure and it is much quicker and simpler than using the public/private key methods for every message.

5. What is it?
    - The client sends the HMAC of all the handshake messages.
    
    Why is it necessary?
    - This is the check to ensure that there was no tampering with any of the messages. If the HMAC is correct, the server will know that it has seen all of the same messages as the client.

6. What is it?
    - The server sends the HMAC of all the handshake messages.
    
    Why is it necessary?
    - This is the check to ensure that there was no tampering with any of the messages. If the HMAC is correct, the client will know that it has seen all of the same messages as the server.


## I can explain how TLS prevents man-in-the-middle attacks.
The first part of TLS that is important in preventing man-in-the-middle attacks is the TLS handshake. This is explained in detail in LO2. This ensures that the messages that are being sent between the client and server cannot be read by the man-in-the-middle.

A potential threat that still remains is for the man-in-the-middle to record messages sent and to resend them to act like the client or server in a replay attack. TLS uses sequence numbers before encryption to counter this. When the client or server receives these messages, they will decrypt the message and see that the sequence number is incorrect and will discard the message.

Sequence numbers do not prevent a connection replay attack. A counter replay attack is when Trudy sniffs all the messages that are sent from the client to the server and uses all of these messages in the same order at another time. The nonces in step 1 and 2 prevent this because if the sender reuses the messages, the nonce will be something that has been used before and the receiver will know that they are not communicating with the correct person.

There is another method of man-in-the-middle attack, and it is when the man-in-the-middle ends a TCP connection early by sending a TCP FIN. TLS prevents this by making the client or server indicate in the type field if they are about to end the TLS session. If the client or server receives a TCP FIN without having received an indication in the type field that the session would be ending, they would know that something is wrong.