# Review Questions

### R20. In the TLS record, there is a field for TLS sequence numbers. True or false?
False. The TLS record has a type field, version field, length field, data field, and HMAC field.

### R21. What is the purpose of the random nonces in the TLS handshake?
The nonces in the TLS handshake are to ensure that another entity cannot reuse messages that were alread sent to pretend to be someone they are not. For example, if a third party were to sniff the packets that were went to and from a client for the online purchase of a hat, if there were not random nonces, this third party could pretend to be the client the next day and send all the same messages that the user sent the previous day, and would purchase another hat. This is called a connection replay attack. Nonces stop this because they are only used once, so if the client and server were to send the same information back and forth a second time, the messages themselves would be different meaning that the server could tell that it was not actually the client sending the messages because the encryption key would be different.

### R22. Suppose an TLS session employs a block cipher with CBC. True or false: The server sends to the client the IV in the clear.
True. Initialization vectors are sent in the clear before encrypting messages.

### R23. Suppose Bob initiates a TCP connection to Trudy who is pretending to be Alice. During the handshake, Trudy sends Bob Alice’s certificate. In what step of the TLS handshake algorithm will Bob discover that he is not communicating with Alice?
Bob will discover that he is not communicating with Alice when he is unable to send and receive properly encrypted messages with Trudy. Bob will realize it is not Alice because Bob sent the Pre-Master Secret encrypted with Alice's public key which he extracted from the certificate. Trudy will not be able to decrypt the Pre-Master Secret properly because she doesn't have access to Alice's public key. Thus when Trudy attempts to create the two encryption and two HMAC keys, she will get the wrong keys, so the encryption and decryption of the messages between Bob and Trudy will not be effective.

### P9. In this problem, we explore the Diffie-Hellman (DH) public-key encryption algorithm, which allows two entities to agree on a shared key. The DH algorithm makes use of a large prime number p and another large number g less than p. Both p and g are made public (so that an attacker would know them). In DH, Alice and Bob each independently choose secret keys , Sa and Sb, respectively. Alice then computes her public key, Ta, by raising g to Sa and then taking mod p. Bob similarly computes his own public key Tb by raising g to Sb and then taking mod p. Alice and Bob then exchange their public keys over the Internet. Alice then calculates the shared secret key S by raising Tb to Sa and then taking mod p. Similarly, Bob calculates the shared key S' by raising Ta to Sb and then taking mod p.

**a.** Prove that, in general, Alice and Bob obtain the same symmetric key, that is, prove S = S'.
$$ T_a = g^{S_a} \mod p $$

$$ T_b = g^{S_b} \mod p $$

$$ S = T_b^{S_a} \mod p = (g^{S_b} \mod p)^{S_a} \mod p = (g^{S_b})^{S_a} \mod p = g^{S_a*S_b} $$

$$ S' = T_a^{S_b} \mod p = (g^{S_a} \mod p)^{S_b} \mod p = (g^{S_a})^{S_b} \mod p = g^{S_a*S_b} $$

**b.** With p=11 and g=2, suppose Alice and Bob choose private keys Sa = 5 and Sb = 12, respectively. Calculate Alice’s and Bob’s public keys, Ta and Tb. Show all work.

$$ T_a = g^{S_a} \mod p = 2^5 \mod 11 = 32 \mod 11 = 10 $$

$$ T_b = g^{S_b} \mod p = 2^{12} \mod 11 = 4096 \mod 11 = 4 $$

**c.** Following up on part (b), now calculate S as the shared symmetric key. Show all work.

$$ S' = T_a^{S_b} \mod p = 10^{12} \mod 11 = 1,000,000,000,000 \mod 11 = 1 $$

$$ S = T_b^{S_a} \mod p = 4^5 \mod 11 = 1024 \mod 11 = 1 $$

**d.** Provide a timing diagram that shows how Diffie-Hellman can be attacked by a man-in-the-middle. The timing diagram should have three vertical lines, one for Alice, one for Bob, and one for the attacker Trudy.

<img src='TimingDiagram.png' width='260' height='296'>

In this case, Trudy will be able to decrypt the messages from Alice, encrypt them with her own key and send them to Bob. Then Bob will encrypt his responses and send them to Trudy. Trudy will decrypt them and encrypt them with her key and send them to Alice.

### P14. The OSPF routing protocol uses a MAC rather than digital signatures to provide message integrity. Why do you think a MAC was chosen over digital signatures?
I think MAC was chosen over digital signatures because the MAC address is built-in on devices, so you cannot spoof the MAC address you are sending from. Since no two adapters have the same MAC, there should be no issue telling which frames came from which sources.

### P23. Consider the example in Figure 8.28. Suppose Trudy is a woman-in-the-middle, who can insert datagrams into the stream of datagrams going from R1 and R2. As part of a replay attack, Trudy sends a duplicate copy of one of the datagrams sent from R1 to R2. Will R2 decrypt the duplicate datagram and forward it into the branch-office network? If not, describe in detail how R2 detects the duplicate datagram.
R2 will not decrypt the message and send it to the branch-office network because messages have sequence numbers. If Trudy does not increment the sequence number, then R2 will discard the message because it already received it. If Trudy does increment the sequence number, R2 will still detect the change. This is because the message is sent along with a MAC. When R2 checks to ensure that the MAC is correct, it will detect an error and know that the message is not proper and will drop it.