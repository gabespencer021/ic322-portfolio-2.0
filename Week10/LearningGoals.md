# Learning Goals

## I can explain what autonomous systems are and their significance to the Internet.
Autonomous systems are groups of routers which are all under the same administrative control. They are typically linked to each other via the same routing protocol. They break the Internet up into different sections. They also enable whatever administration controls them to set up their own AS however they see fit for the best functionality. This means that they are not all the same. Often ISPs and their routers and connections make up one AS, but sometimes an ISP will break their networks into multiple ASs. Every AS has a unique autonomous system number (ASN).

ASs make it so that routers can communicate with each other effectively. Within a given AS, all the routers have the same routing protocol, so there should be no issue with communication there. It has been established that for communication between ASs, Border Gateway Protocol (BGP) will be used, and having clearly-defined ASs makes it clear how routers that are not under the same administrative control should communicate with each other because they may be running different routing protocols.

## I can describe how the BGP protocol works as well as why and where it is used.
**What is BGP**
BGP (Border Gateway Protocol) is the universal inter-AS routing protocol. All ASs run the same thing because this allows them all to communicate with each other without issue. BGP is used both to send datagrams and for routers to to advertise their presence. It is how ISPs are connected to each other.

**How does BGP work**
BGP uses a distance-vector algorithm to determine the path on which datagrams should be sent. This means that the routers have to receive information from their neighbors about paths to other routers. BGP advertises the presence of routers using eBGP (external BGP) and iBGP (internal BGP) messages. Each message contains a destination (where the message originated), a NEXT-HOP, and an AS-PATH attribute. In reality, it has other fields as well, but these are the relevant ones. The NEXT-HOP attribute stores the closest router on the path that is outside the current AS. The AS-PATH attribute is a list of the different ASs that must be traversed in order to reach the destination. eBGP is when a router is sending a message to a router in another AS. Each router that is sending an eBGP message will take what it received and will add its own AS onto the front of the list as well as making itself the NEXT-HOP so that the receiving AS knows the proper NEXT-HOP and AS-PATH. Each router that is sending an iBGP message will simply send the message that it received with no need to change it because the information for all routers on the AS is the same. The method that BGP uses to decide the best route to send datagrams based on the destination has four steps.
1. A route is assigned a local preference
    - these are assigned by the network administrator for the AS. 
    - The routes with the highest local preference values are selected
2. From the remaining routes, the route with the shortest AS-PATH is chosen
    - This resembles a DV algorithm, but using AS hops instead of router hops
3. From the remaining routes, the route with the closest NEXT-HOP is chosen
    - The goal is to get the datagram to the other AS as fast as possible
4. If more than one route still remains, the router uses BGP identifiers to select one
    - We have not explored BGP identifiers (yet?)

## I can explain what the ICMP protocol is used for, with concrete examples.
ICMP (Internet Control Message Protocol) is how hosts and routers communicate network-layer information with each other, most frequently error messages, though they have many other uses as well. ICMP messages are carried as payload of IP messages. ICMP messages have a type and code field as well as carrying the header and the first 8 bits of the IP datagram that caused the ICMP message to be sent. An example of ICMP being used is the  `ping` command. It sends a type 8 code 0 ICMP message (echo request), and should receive a response of type 0 code 0 (echo reply).

Table of typical ICMP typesm, codes, and their descriptions:

| ICMP Type | Code | Description |
| :-------- | :--- | :---------- |
| 0 | 0 | echo reply (to ping) |
| 3 | 0 | destination network unreachable |
| 3 | 1 | destination host unreachable |
| 3 | 2 | destination protocol unreachable |
| 3 | 3 | destination port unreachable |
| 3 | 6 | destination network unknown |
| 3 | 7 | destination host unknown |
| 4 | 0 | source quench (congestion control) |
| 8 | 0 | echo request |
| 9 | 0 | router advertisement |
| 10 | 0 | router discovery |
| 11 | 0 | TTL expired |
| 12 | 0 | IP header bad |

Another common command that uses ICMP is `traceroute`. The goal of this command is to find out the complete path to a certain destination. It sends IP datagrams with ICMP messages within them to the destination's IP address. They also carry a UDP segment with an unlikely port number. The first datagram has a TTL of 1, the second of 2, the third 3, and so on. This causes a datagram to expire at each router along the path to the destination, and so each router will send an ICMP error message back to the sender because the TTL has expired (type 11 code 0). It will do this until a datagram reaches the destination at which time the source should receive an ICMP type 3 code 3 message. This is the source's cue to stop sending datagrams.