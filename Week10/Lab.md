# Protocol Pioneer Chapter 5

## Introduction

This lab was redoing Protocol Pioneer Chapter 5 to make it better.

## Collaboration

I collaborated with Soleil for this lab.

## Process

I followed the instructions on the [course website](https://ic322.com/#/assignments/week07/protocol-pioneer/) for this lab.

## Questions

1. How did you solve the Chapters? Please copy and paste your winning strategy(s), and also explain it in English.

The solution for this involved using a DV algorithm in order to determine the shortest route for messages to go to reach their desired destination. In this scenario, Sawblade, the player, and each drone would have a distance vector. If they received a message with a destination, they would forward it on that turn. Otherwise, they would send a message that contained their own DV to one of their neighbors, selected at random. Once a message was received, it would be parsed and if it were a Data message, it would be forwarded, but if it were a DV message, the player, Sawblade, or the drone would update their own DV based on that DV they just received. Eventually, each drone will find the shortest path to a destination and when it receives a message, it will send it on that path.

```
import json

def create_message(src, dest, type, payload):
    """If `payload` is a string, it will come back out as a string. If it's a
    Dict, it will come back out as a dict."""
    
    m = {
        "Source": src,
        "Destination": dest,
        "Type": type,
        "Payload": payload
    }

    return json.dumps(m)

def parse_message(m):
    parsed = json.loads(m)
    return {
        "Source": parsed["Source"],
        "Destination": parsed["Destination"],
        "Type": parsed["Type"],
        "Payload": parsed["Payload"]
    }

def create_dv_message(src, dest, dv):
    """dv should be a Python Dict. src and dest should be strings."""
    dv_string = json.dumps(dv)
    return create_message(src, dest, "DV", dv)


def update_dv(old_dv, new_dv, iface):
    """Updates old_dv based on information from new incoming dv. iface is the interface
    that recieved the mesage."""
    for id in new_dv.keys():
        if id not in old_dv.keys():
            # We discovered a new node
            old_dv[id] = [
                new_dv[id][0] + 1,
                iface
            ]
        else:
            # We have an update for a previously known node. Update our
            # dv database if it's a shorter path.
            new_value = new_dv[id][0] + 1
            if old_dv[id][0] > new_value:
                old_dv[id][0] = new_value
                old_dv[id][1] = iface
        

def create_key_msg(src, dest, key, exp):
    """key and exp should be strings or ints."""
    
    key_string = {"Key":key, "Expiration":exp}
    return create_message(src, dest, "Data", key_string)


import random

# Welcome to the StellarScript Console!
#
# In this chapter you have 3 strategies to program: yourself, the drones, and Sawblade.
# 
# Nodes have random IDs in this level. They change every time you restart the level, but they're constant during
# a single play-through.


def drone_strategy(self):
    """Drones are responsible for routing messages."""

    # This is our distance vector dict that keeps track of distances to different nodes, and the interface for
    # which that path exists. "X" means no interface (since it's me). My own id should be the only one with an "X"
    if "DV" not in self.state:
        self.state["DV"] = { self.id: [0, "X"] }
        

    # We can only send one message per turn, so let's keep track.
    sent_message = False
    
    # The `self.connected_interfaces()` method allows you to see what interfaces are connected to wires.
    while(self.message_queue):
        m = self.message_queue.pop()
        parsed = parse_message(m.text)
        # print(f"--- Drone {id}: Msg on interface {m.interface} ---\n{m.text}\n------------------")
        if parsed["Type"] == "DV":
            # handle DV packets
            update_dv(self.state["DV"], parsed["Payload"], m.interface)
            # print(f"{id} DV packet received. New DV: {self.state['DV']}")
        elif parsed["Type"] == "Data":
            # handle data packets
            if parsed["Destination"] in self.state["DV"].keys():
                # We know of a route (we're not 100% sure it's the shortest route tho)
                dest = parsed["Destination"]
                iface = self.state["DV"][dest][1]
                self.send_message(m.text, iface)
                sent_message = True
                


    # Send DV packet to neighbors. Source and destination are ignored since DV packets aren't forwarded.
    # Only send if no other message was already sent (data packets get priority)
    if not sent_message:
        m = create_dv_message("0", "0", self.state["DV"])
        interface = random.choice(self.connected_interfaces())
        self.send_message(m, interface)


def player_strategy(self):
    """This is you. You have access to the key-generating machine. You need to send that key to Sawblade."""

    # We can only send one message per turn, so let's keep track.
    sent_message = False
    
    # ---------------- Key Section ------------------
    if "ticks" not in self.state:
        self.state["ticks"] = 1
    else:
        self.state["ticks"] += 1
    # print(f"The current time is {self.state['ticks']}...")

    if self.state["ticks"] % 20 == 0:
        key_expiration = self.generate_key()
        key = key_expiration[0]
        expiration = key_expiration[1]
        print(f"Player: key {key} expires at {expiration}.")
        m = create_key_msg(src="1", dest="2", key=key, exp=expiration)
        self.send_message(m, "E")
        sent_message = True
        


    # -------------DV Section ---------------------
    
    # This is our distance vector dict that keeps track of distances to different nodes, and the interface for
    # which that path exists. "X" means no interface (since it's me). My own id should be the only one with an "X"
    if "DV" not in self.state:
        self.state["DV"] = { self.id: [0, "X"] }
        
    
    # The `self.connected_interfaces()` method allows you to see what interfaces are connected to wires.
    while(self.message_queue):
        m = self.message_queue.pop()
        parsed = parse_message(m.text)
        if parsed["Type"] == "DV":
            # handle DV packets
            update_dv(self.state["DV"], parsed["Payload"], m.interface)
            # print(f"{id} DV packet received. New DV: {self.state['DV']}")
        elif parsed["Type"] == "Data":
            # handle data packets
            if parsed["Destination"] in self.state["DV"].keys():
                # We know of a route (we're not 100% sure it's the shortest route tho)
                dest = parsed["Destination"]
                iface = state["DV"][dest][1]
                self.send_message(m.text, iface)
                sent_message = True
                


    # Send DV packet to neighbors. Source and destination are ignored since DV packets aren't forwarded.
    # Only send if no other message was already sent (data packets get priority)
    if not sent_message:
        m = create_dv_message("0", "0", self.state["DV"])
        interface = random.choice(self.connected_interfaces())
        self.send_message(m, interface)

def sawblade_strategy(self):
    """Sawblade operates the radio, but need an unexpired key."""

    # We can only send one message per turn, so let's keep track.
    sent_message = False

    # ---------------- Time Section ------------------
    if "ticks" not in self.state:
        self.state["ticks"] = 1
    else:
        self.state["ticks"] += 1
    
  # -------------DV Section ---------------------
    
    # This is our distance vector dict that keeps track of distances to different nodes, and the interface for
    # which that path exists. "X" means no interface (since it's me). My own id should be the only one with an "X"
    if "DV" not in self.state:
        self.state["DV"] = { self.id: [0, "X"] }
        
    
    # The `self.connected_interfaces()` method allows you to see what interfaces are connected to wires.
    while(self.message_queue):
        m = self.message_queue.pop()
        print(f"--- Sawblade: Msg on interface {m.interface} ---\n{m.text}\n------------------")
        parsed = parse_message(m.text)
        # print(f"--- Drone {id}: Msg on interface {m.interface} ---\n{m.text}\n------------------")
        if parsed["Type"] == "DV":
            # handle DV packets
            update_dv(self.state["DV"], parsed["Payload"], m.interface)
            # print(f"{id} DV packet received. New DV: {self.state['DV']}")
        elif parsed["Type"] == "Data":
            # handle data packets
            if parsed["Destination"] == self.id:
                k = parsed["Payload"]["Key"]
                e = parsed["Payload"]["Expiration"]
                t = self.state["ticks"]
                print(f"Sawblade receives the key {k}. She pauses, counts her heartbeats. '{t} beats so far.' She looks at the key. It expires at {e}.")
                if t <= t:
                    # Not expired
                    self.operate_radio(k)
                else:
                    print(f"Dang. Expired.")
                


    # Send DV packet to neighbors. Source and destination are ignored since DV packets aren't forwarded.
    # Only send if no other message was already sent (data packets get priority)
    if not sent_message:
        m = create_dv_message("0", "0", self.state["DV"])
        interface = random.choice(self.connected_interfaces())
        self.send_message(m, interface)
        

s = Chapter5(drone_strategy, player_strategy, sawblade_strategy, animation_frames=1, wait_for_user=False)
s.run()
```

2. Include a section on Beta Testing Notes.

- List any bugs you find. When you list the bugs, try to specify the exact conditions that causes them so I can reproduce them.
    - I did not find any bugs.
- Was there anything that you and others were consistently confused about? What would you do to solve that confusion?
    - I did not find anything confusing
- Was there too much narrative? Not enough narrative?
    - Good amount of narrative
- Was it fun?
    - This week was pretty fun, although it felt a little bit easy because we cheated a little bit.
- Really, just let me know how I can improve the game.
    - No particular recommendations