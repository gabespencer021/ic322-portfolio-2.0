# Review Questions

## R11. How does BGP use the NEXT-HOP attribute? How does it use the AS-PATH attribute?
The NEXT-HOP attribute is the name of the nearest router on the path to the destination that is in a different AS than the current router. It uses BGP in the case when there are multiple ASs that are on the path from the current router to the destination to determine which AS is closer to the current router. If two different paths to the destination both have the same number of ASs, then the NEXT-HOP that has the lowest cost will be chosen.

The AS-PATH attribute is used to determine which route among many has the fewest ASs that the datagram would have to travel through in order to reach the destination. When selecting a route, the router will select whichever one has fewer ASs in its AS-PATH attribute.

## R13. True or false: When a BGP router receives an advertised path from its neighbor, it must add its own identity to the received path and then send that new path on to all of its neighbors. Explain.
If the router is a gateway router and it is advertising the path to another AS that is not its own AS with an eBGP message, then it will add its own AS to the path and then forward it to the new AS. If the router is not a gateway router or is a gateway router that is only sending iBGP messages and no eBGP messages, then it will not add anything to the path before it forwards it to all its neighbors. In short, any iBGP message will not add its own AS to the path, but any eBGP message will.

## R19. Names four different types of ICMP messages (also describe what they are used for)
- Type 0 code 0: echo reply (to `ping`)
- Type 8 code 0: echo request
- Type 9 code 0: router advertisement
- Type 10 code 0: router discovery

## R20. What two types of ICMP messages are received at the sending host executing the Traceroute program?
- Type 11 code 0: TTL expired
    - received when the TTL expired at a router
- Type 3 code 3: port unreachable
    - received when the datagram arrives at the destination

## P14. Consider the network shown below. Suppose AS3 and AS2 are running OSPF for their intra-AS routing protocol. Suppose AS1 and AS4 are running RIP for their intra-AS routing protocol. Suppose eBGP and iBGP are used for the inter-AS routing protocol. Initially suppose there is no physical link between AS2 and AS4.
![P14 Image](Week10/P14.png)

**a. Router 3c learns about prefix *x* from which routing protocol: OSPF, RIP, eBGP, or iBGP?**<br>
eBGP

**b. Router 3a learns about *x* from which routing protocol?**<br>
iBGP

**c. Router 1c learns about *x* from which routing protocol?**<br>
eBGP

**d. Router 1d learns about *x* from which routing protocol?**<br>
iBGP

## P15. Referring to the previous problem, once router 1d learns about *x* it will put an entry (*x*,*I*) in its forwarding table.

**a. Will *I* be equal to *I<sub>1</sub>* or *I<sub>2</sub>*  for this entry? Explain why in one sentence.**
*I* will be equal to *I1* because that provides a shorter NEXT-HOP route to AS3 which is the next AS to connect to *x*.

**b. Now suppose that there is a physical link between AS2 and AS4, shown by the dotted line. Suppose router 1d learns that *x* is accessible via AS2 as well as via AS3. Will *I* be set to *I<sub>1</sub>* or *I<sub>2</sub>*? Explain why in one sentence.**
*I* will be set to *I<sub>2</sub>* because the NEXT-HOP along *I<sub>2</sub>* is 3 while the NEXT-HOP for *I<sub>1</sub>* is 3.

**c. Now suppose there is another AS, called AS5, which lies on the path between AS2 and AS4 (not shown in diagram). Suppose router 1d learns that *x* is accessible via AS2 AS5 AS4 as well as via AS3 AS4. Will I be set to *I<sub>1</sub>* or *I<sub>2</sub>*? Explain why in one sentence.**
*I* will be set to *I<sub>1</sub>* because the AS-PATH that goes through AS3 is shorter than the one that goes through AS2 and AS5.

## P19. In Figure 5.13, suppose that there is another stub network V that is a customer of ISP A. Suppose that B and C have a peering relationship, and A is a customer of both B and C. Suppose that A would like to have the traffic destined to W to come from B only, and the traffic destined to V from either B or C. How should A advertise its routes to B and C? What AS routes does C receive?
A should tell B that it has a route to W, but should not tell C that it has a route to W. It should tell both B and C that it has a route to V. In this case C will receive the from A that A has a route to V, and from B that B has routes to both A and V and W so that if C wants to send anything to W, it will send it to B and then all of the traffic for W will come through B like A wants.

## P20. Suppose ASs X and Z are not directly connected but instead are connected by AS Y. Further suppose that X has a peering agreement with Y, and that Y has a peering agreement with Z. Finally, suppose that Z wants to transit all of Y’s traffic but does not want to transit X’s traffic. Does BGP allow Z to implement this policy?​
No, Z cannot implement this. If Z wants to be able to have Y's traffic, then it will use BGP to advertise itself to Y. Then Y will advertise both itself and Z to X because they have a peering relationship. So, if Z tells Y about itself, it is naturally telling X about itself too.