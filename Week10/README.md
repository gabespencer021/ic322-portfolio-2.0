# Week 10: The Network Layer: More Control Plane

*Gabriel Spencer | Fall 2023*

This week

* [Review Questions](Week10/ReviewQs.md)
* [Learning Goals](Week10/LearningGoals.md)
* [Lab: Beta Tester](Week10/Lab.md)
* [Back to Overview](../README.md)
